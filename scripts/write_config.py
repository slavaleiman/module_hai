#!/usr/bin/python3

								#addr   output chan1   chan2
#./write_config.py /dev/ttyACM0 60 		1 		1 	 	8

from modbus import *

if len(sys.argv) > 4:
    set_port(sys.argv[1])
    device_addr = sys.argv[2]
    output = sys.argv[3] # pair num
    uchan = sys.argv[4]
    ichan = sys.argv[5]
else:
    print("USAGE: %s PORT DEVICE_ADDR OUTPUT CHANNEL1 CHANNEL2\n"
		+ "EXAMPLE:\n./write_config.py /dev/ttyACM0 60 		1 		1 	 	8")
    	
    exit(0)

pairs = [
    36,
    45,
    54,
    63,
    72,
    81
]

print(pairs[int(output,10)])
ret = write_holding_reg(device_addr, int(pairs[int(output,10)]), int(uchan), sleep_time=0.05)
print(ret)
ret = write_holding_reg(device_addr, int(pairs[int(output,10)]+1), int(ichan), sleep_time=0.05)
print(ret)
sleep(0.5)

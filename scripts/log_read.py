#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import serial
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s [port]" % sys.argv[0])
    sys.exit(1)

ser = serial.Serial(sys.argv[1], sys.argv[2])
#ser = serial.Serial(sys.argv[1], baudrate=1200)

if(ser.isOpen() == False):
    ser.open()

count = 13

while 1 :
    out = ''
    seq = []
    time.sleep(0.1)
    while ser.inWaiting() > 0:
        for c in ser.read():
            # seq.append(c)
            seq.append(chr(c)) # convert from ANSII
            out = ''.join(str(v) for v in seq) # make a string from array
    if out != '':
        print(out)

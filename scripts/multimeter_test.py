#!/usr/bin/python3
from modbus import *

if len(sys.argv) > 0:
    set_port(sys.argv[1])
    device_addr = sys.argv[2]
    # regnum = sys.argv[3]
else:
    print("USAGE: %s PORT DEVICE_ADDR CHANNEL")
    exit(0)

output_regs = [
    36,
    45,
    54,
    63,
    72,
    81
]

# def set_delta_for_channels():
# write_holding_reg(device_addr, 1, 5001) # 0
write_holding_reg(device_addr, 91, 0xFFFF - 103) # 0
write_holding_reg(device_addr, 94, 0xFFFF - 146) # 3
write_holding_reg(device_addr, 96, 0xFFFF - 132) # 5

def twos_complement(hexstr,bits):
        value = int(hexstr,16)
        if value & (1 << (bits-1)):
            value -= 1 << bits
        return value


zero_lvl = 1.65

while True:
    i = 0
    output = ''
    ret = read_holding_reg(device_addr, 0, 34)
    if ret:
        output += 'rate chan:\t{:8}\nrate:\t\t{:8}\ndefault rate:\t{:8}\nchannels: \n\tenabled:{:8}\n\tready:\t{:8}\n'.format(int(ret[1][9:14].replace(" ",""), 16),
                                                                                        int(ret[1][15:20].replace(" ",""), 16),
                                                                                        int(ret[1][21:26].replace(" ",""), 16),
                                                                                        ret[1][27:32].replace(" ",""),
                                                                                        ret[1][33:38].replace(" ",""))
        
        for chan in range(0,14):
            l1 = 39 + chan * 12
            r1 = l1 + 5
            l2 = 45 + chan * 12
            r2 = l2 + 5
            output += '{:2} rms: {:8.4}v avg: {:8.4}v val:{:5}\n'.format(chan, 
            # output += '{} rms: {:8} avg: {:8}\n'.format(chan, 
                                                    twos_complement(ret[1][l1:r1].replace(" ", ""), 16) * 3.3 / 4096, 
                                                    twos_complement(ret[1][l2:r2].replace(" ", ""), 16) * 3.3 / 4096,
                                                    twos_complement(ret[1][l2:r2].replace(" ", ""), 16))
                                                    # twos_complement(ret[1][l1:r1].replace(" ", ""), 16), 
                                                    # twos_complement(ret[1][l2:r2].replace(" ", ""), 16))
        lvbat = 39 + 14 * 12
        rvbat = lvbat + 5
        output += 'vbat: {}\n'.format(4 * 3.3 * int(ret[1][lvbat:rvbat].replace(" ", ""), 16) / 0xFFF)

    output_chan = 0
    for regnum in output_regs:
        ret = read_holding_reg(device_addr, int(regnum), 9)
        # output += ret[1] + '\n'
        if ret:
            output_chan += 1
            l1 = 9
            r1 = 14
            output += '{:2} '.format(output_chan) +\
                'UCHANNEL:{:2}  '.format(int(ret[1][9:14].replace(" ",""), base=16)) +\
                'ICHANNEL:{:2}  '.format(int(ret[1][15:20].replace(" ",""), base=16)) +\
                'P: {:10}  '.    format(int((ret[1][21:26] + ret[1][27:32]).replace(" ",""), base=16)) +\
                'S: {:10}  '.    format(int((ret[1][45:50] + ret[1][51:56]).replace(" ",""), base=16)) +\
                'Q: {:10}  '.    format(int((ret[1][33:38] + ret[1][39:44]).replace(" ",""), base=16)) +\
                'cosFi: {:8}\n'.format(twos_complement(ret[1][57:62].replace(" ", ""), 16) / 10000.0)

    ret = read_holding_reg(device_addr, 90, 1) # zero level
    if ret:
        zero_lvl = int(ret[1][9:14].replace(" ",""), 16) * 3.3 / 4096
        output += 'adc zero {}v\n'.format(zero_lvl)
    system('clear')
    print(output)
    sleep(0.5)

#!/usr/bin/python3

from modbus import *

if len(sys.argv) > 0:
    set_port(sys.argv[1])
    device_addr = sys.argv[2]
else:
    print("USAGE: %s PORT DEVICE_ADDR")
    exit(0)

write_holding_reg(device_addr, 91, 1, sleep_time=0.05)

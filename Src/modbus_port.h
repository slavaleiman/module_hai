#ifndef __MODBUS_PORT__
#define __MODBUS_PORT__ 1

#include <string.h>
#include <stdbool.h>
#include "circular_buffer.h"
#include "device_data.h"

#ifndef UNITTEST
#include "stm32f7xx.h"
#endif
#define DMA_RX_BUFFER_SIZE          257
#define PORT_BUFFER_SIZE           	257

typedef struct modbus_port_s modbus_port_t;

struct modbus_port_s
{
#ifndef	UNITTEST
	USART_TypeDef*		usart;
	DMA_Stream_TypeDef*	dma_input_stream;
	uint32_t 			dma_input_stream_channel;
	DMA_Stream_TypeDef* dma_output_stream;
	uint32_t 			dma_output_stream_channel;
	GPIO_TypeDef*		de_port;
	uint32_t			de_pin;
    uint32_t 			baudrate;
#endif
    bool 				is_sending;
	uint8_t 			dma_rx_buffer[DMA_RX_BUFFER_SIZE];
	circular_buffer_t* 	cbuffer;

    uint8_t     		rx_buffer[PORT_BUFFER_SIZE];
    uint8_t     		rx_size;
    uint8_t     		tx_buffer[PORT_BUFFER_SIZE];
    uint8_t     		tx_size;

    modbus_device_t* 	device;
};

void mbport_process(modbus_port_t* mbport);
#ifndef UNITTEST

void mbport_init(modbus_port_t* mbport,
					USART_TypeDef *USART,
					DMA_Stream_TypeDef *dma_input_stream,
					uint32_t dma_input_stream_channel,
					DMA_Stream_TypeDef *dma_output_stream,
					uint32_t dma_output_stream_channel,
				    uint32_t 	baudrate,
					circular_buffer_t* cbuffer,
					modbus_device_t* mb_device,
					GPIO_TypeDef* de_port,
					uint16_t de_pin
					);
#endif
void mbport_transmit(modbus_port_t* mbport);
void usart_irq_handler(modbus_port_t* mbport);
void usart_on_dma_interrupt(DMA_Stream_TypeDef* dma_stream);

#endif

#ifndef _UNITTEST_
#include "stm32f746xx.h"
#endif

#include "multimeter.h"
#include "math.h"
#include "errors.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#ifndef _UNITTEST_
const uint32_t adc_in_by_channel[15] = {
    ADC_CHANNEL_10,
    ADC_CHANNEL_11,
    ADC_CHANNEL_12,
    ADC_CHANNEL_13,
    ADC_CHANNEL_1,
    ADC_CHANNEL_3,
    ADC_CHANNEL_4,
 
    ADC_CHANNEL_5,
    ADC_CHANNEL_6,
    ADC_CHANNEL_7,
    ADC_CHANNEL_14,
    ADC_CHANNEL_15,
    ADC_CHANNEL_8,
    ADC_CHANNEL_9,

    ADC_CHANNEL_18 // VBAT
};
#endif
multimeter_t multimeter;

bool adc_ready = false;

uint16_t* multimeter_buffer(void)
{
    return multimeter.dma_buffer;
}

void multimeter_set_actual_buffer(uint16_t* buffer)
{
    multimeter.actual_data = buffer;
}

float rms(uint16_t* buffer, uint8_t rank, uint8_t num_channels, uint16_t period, uint8_t channel)
{
    uint32_t output = 0;
    uint32_t zero_level = multimeter.adc_zero_level 
                            + multimeter.delta[channel];

    if(period && (period <= MM_NUM_SAMPLES))
    {
        for(uint16_t sample_num = 0; sample_num < period; ++sample_num)
        {
            int32_t value = (buffer[rank + num_channels * sample_num] - zero_level);
            output += value * value;
        }
    }
    return sqrtf(output / period);
}

//Urms * Irms; // полная мощность
static void calculate_S(uint8_t rank, uint16_t period)
{
    uint8_t uchannel = multimeter.sequence[rank][0];
    float Urms = rms(multimeter.actual_data, 2 * rank, multimeter.seq_len * 2, period, uchannel);

    uint8_t ichannel = multimeter.sequence[rank][1];
    float Irms = rms(multimeter.actual_data, 2 * rank + 1, multimeter.seq_len * 2, period, ichannel);
    
    uint8_t out_channel = multimeter.sequence[rank][2];

    multimeter.urms[uchannel] = Urms;
    multimeter.urms[ichannel] = Irms;

    float output = Urms * Irms;
    multimeter.S[out_channel] = output;
}

static void calculate_P(uint8_t rank, uint16_t period)
{
    uint16_t* data = multimeter.actual_data;

    int32_t output = 0;
    uint8_t uchannel = multimeter.sequence[rank][0];
    uint32_t uzero_level = multimeter.adc_zero_level 
                            + multimeter.delta[uchannel];
    uint8_t ichannel = multimeter.sequence[rank][1];
    uint32_t izero_level = multimeter.adc_zero_level
                            + multimeter.delta[ichannel];
    if(period && (period <= MM_NUM_SAMPLES))
    {
        for(uint16_t sample_num = 0; sample_num < period; ++sample_num)
        {
            uint32_t u_offset = 2 * (sample_num * multimeter.seq_len + rank);
            uint32_t i_offset = u_offset + 1;
            output += (data[u_offset] - uzero_level)
                    * (data[i_offset] - izero_level); // full
        }
        uint8_t out_channel = multimeter.sequence[rank][2];

        multimeter.P[out_channel] = output / period;
    }
}

// Q = sqrt(S^2 - P^2); // реактивная мощность
static void calculate_Q(uint8_t rank)
{
    uint8_t out_channel = multimeter.sequence[rank][2];

    if(multimeter.S[out_channel] >= multimeter.P[out_channel])
        multimeter.Q[out_channel] = sqrtf(multimeter.S[out_channel] * multimeter.S[out_channel] - multimeter.P[out_channel] * multimeter.P[out_channel]);
    else
        multimeter.Q[out_channel] = -1;
}

static void calculate_cosFI(uint8_t rank)
{
    uint8_t out_channel = multimeter.sequence[rank][2];

    if(multimeter.S[out_channel])
        multimeter.cosFI[out_channel] = (float)multimeter.P[out_channel] / multimeter.S[out_channel];
}

int32_t avg(uint16_t* buffer, uint8_t offset,  uint16_t period, uint8_t ch)
{
    int32_t output = 0;
    uint32_t zero_level = multimeter.adc_zero_level 
                            + multimeter.delta[ch];
    if(period && period <= MM_NUM_SAMPLES)
    {
        for(uint16_t sample_num = 0; sample_num < period; ++sample_num)
        {
            int32_t value = buffer[sample_num * multimeter.seq_len * 2 + offset] - zero_level;
            output += value;
        }
        return output / period;
    }
    return 0;
}

void calculate_avg(uint8_t rank)
{
    uint8_t offset = rank * 2;
    uint8_t uchannel = multimeter.sequence[rank][0];
    multimeter.uavg[uchannel] = avg(multimeter.actual_data, 
                                            offset, 
                                            multimeter.period,
                                            uchannel);
    // for paired rank
    offset = rank * 2 + 1;
    uint8_t ichannel = multimeter.sequence[rank][1];
    multimeter.uavg[ichannel] = avg(multimeter.actual_data, 
                                            offset,
                                            multimeter.period,
                                            ichannel);
 
    multimeter.channel_ready_flag |= 1 << uchannel; 
    multimeter.channel_ready_flag |= 1 << ichannel; 
}

void calculate_vbat(uint8_t rank)
{
    uint8_t offset = rank * 2;
    int32_t output = 0;
    uint16_t* data = multimeter.actual_data;
    if(multimeter.period && multimeter.period <= MM_NUM_SAMPLES)
    {
        for(uint16_t sample_num = 0; sample_num < multimeter.period; ++sample_num)
        {
            output += data[sample_num * multimeter.seq_len * 2 + offset];
        }
        multimeter.vbat = output / multimeter.period;
    }
    multimeter.vbat_ready = true;
}

void multimeter_update(void)
{
#ifndef _UNITTEST_
    if(multimeter.update_required)
    {
        // debug
        uint32_t t1 = DWT->CYCCNT;
#endif
        if(!multimeter.actual_data)
            return;

        multimeter.channel_ready_flag = 0;
        multimeter.output_ready = 0;
        // last item in seq - always vbat
        uint8_t rank = 0;
        for(; rank < multimeter.seq_len - 1; ++rank)
        {
            if(multimeter.period)
            {
                calculate_P(rank, multimeter.period);
                calculate_S(rank, multimeter.period);
                calculate_Q(rank);
                calculate_cosFI(rank);
                calculate_avg(rank);
                
                uint8_t out_channel = multimeter.sequence[rank][2];
                multimeter.output_ready |= (1 << out_channel);
            }
        }
        calculate_vbat(rank);
        multimeter.update_required = false;
        multimeter.adc_ready = true;

        // debug
#ifndef _UNITTEST_

        uint32_t t2 = DWT->CYCCNT;
        multimeter.process_time_diff = t2 - t1;
    }
#endif
}

bool multimeter_is_ready(void)
{
    return multimeter.adc_ready;
}

void multimeter_clear_ready_flag(void)
{
    multimeter.adc_ready = false;
}

int32_t multimeter_P(uint8_t channel)
{
    return multimeter.P[channel];
}

float multimeter_S(uint8_t channel)
{
    return multimeter.S[channel];
}

float multimeter_Q(uint8_t channel)
{
    return multimeter.Q[channel];
}

float multimeter_cosFI(uint8_t channel)
{
    return multimeter.cosFI[channel];
}

float multimeter_uavg(uint8_t channel)
{
    return multimeter.uavg[channel];
}

float multimeter_urms(uint8_t channel)
{
    return multimeter.urms[channel];
}

#ifndef _UNITTEST_
static void adc_timer_stop(void)
{
    TIM5->CR1  &= ~TIM_CR1_CEN;  // timer enable
}

static void adc_timer_init(void)
{
    // __HAL_RCC_TIM5_CLK_ENABLE();
    SET_BIT(RCC->APB1ENR, RCC_APB1ENR_TIM5EN);

    DBGMCU->APB1FZ |= DBGMCU_APB1_FZ_DBG_TIM5_STOP;

    uint32_t tmpreg = READ_BIT(RCC->APB1ENR, RCC_APB1ENR_TIM5EN);
    (void)tmpreg;
    READ_REG(RCC->APB1ENR);
    // 40 kHz
#ifdef PHASE_LEN_CHANGER
    uint32_t uwTimclock = 216000000;
    uint32_t uwPrescalerValue = (uint32_t)((uwTimclock / 40000) - 1);
    TIM5->PSC = uwPrescalerValue;
    // TIM5->ARR = 40000 - 1;   // 1 Hz // FOR TEST
    // TIM5->ARR = 400 - 1;     // 100 Hz
    TIM5->ARR = 4 - 1;          // 10000 Hz = 50hz * 200 samples
#else

    // uint32_t uwPrescalerValue = (uint32_t)((uwTimclock / 54000000) - 1);
    TIM5->PSC = 4 - 1;
    // TIM5->ARR = 5400000 - 1; // 1 Hz 
    float rate = DEFAULT_RATE * MM_NUM_SAMPLES / 100;
    TIM5->ARR = TIM_BUS_FREQ / rate - 1;
#endif

            // 10000 Hz
    // trigger output
    // interrupt
    TIM5->EGR  |= TIM_EGR_UG;    // event generate
    TIM5->CR2  |= TIM_CR2_MMS_1;    // Update event is used as trigger output (TRGO)
    TIM5->DIER |= TIM_DIER_UIE;     // update interrupt
    TIM5->CR1  |= TIM_CR1_CEN;      // timer enable

    NVIC_SetPriority(TIM5_IRQn, 5);
    NVIC_EnableIRQ(TIM5_IRQn);
    // NVIC_SetPriority(TIM6_DAC_IRQn, 6);
    // NVIC_EnableIRQ(TIM6_DAC_IRQn);
}

// can turn off update interrupt 
void multimeter_on_timer_interrupt(void)
{
    TIM5->SR &= ~TIM_SR_UIF;
}

void multimeter_on_adc_interrupt(void)
{
    if(ADC1->SR & (ADC_SR_EOC | ADC_SR_OVR))
    {
        ADC1->SR &= ~ADC_SR_EOC;
        ADC1->SR &= ~ADC_SR_OVR;
    }
    if(ADC2->SR & (ADC_SR_EOC | ADC_SR_OVR))
    {
        ADC2->SR &= ~ADC_SR_EOC;
        ADC2->SR &= ~ADC_SR_OVR;
    }
}
#endif
void multimeter_on_adc_error(void)
{
#ifndef _UNITTEST_
    errors_on_error(ADC_CONVERSION_ERROR);
#endif
    // reinit ?
}

// for tests
uint16_t multimeter_period(void)
{
    return multimeter.period;
}

// for tests
void multimeter_set_period(uint16_t period)
{
    multimeter.period = period;
}

void multimeter_set_rate(uint32_t rate)
{
#ifdef PHASE_LEN_CHANGER
    if((rate >= 3000) && (rate <= 7000))
    {
        multimeter.period = 1000000 / rate;
    }
#else
    // if TIMER CHANGER
    if((rate >= 3000) && (rate <= 7000))
    {
        TIM5->PSC = 4;
        // TIM5->ARR = 5400000 - 1; // 1 Hz 
        // you have ALWAYS MM_NUM_SAMPLES in buffer
        float r = MM_NUM_SAMPLES * rate / 100;
        TIM5->ARR = TIM_BUS_FREQ / r - 1;
    }
#endif
}

uint16_t multimeter_rate(void)
{
    return TIM_BUS_FREQ * 100.0 / TIM5->ARR / MM_NUM_SAMPLES;
}

uint16_t multimeter_channels_enabled(void)
{
    return multimeter.channel_enable_flag;
}

uint16_t multimeter_channels_ready(void)
{
    return multimeter.channel_ready_flag;
}

#ifndef _UNITTEST_
void config_adc_channel(ADC_TypeDef* _ADC, uint32_t adc_channel, uint32_t rank)
{
    if((adc_channel > ADC_CHANNEL_9) && (adc_channel != ADC_INTERNAL_NONE))
    {
        /* Clear the old sample time */
        _ADC->SMPR1 &= ~ADC_SMPR1(ADC_SMPR1_SMP10, adc_channel);

        if(adc_channel == ADC_CHANNEL_TEMPSENSOR)
        {
            /* Set the new sample time */
            _ADC->SMPR1 |= ADC_SMPR1(ADC_SAMPLETIME_144CYCLES, ADC_CHANNEL_18);
        }
        else
        {
            /* Set the new sample time */
            _ADC->SMPR1 |= ADC_SMPR1(ADC_SAMPLETIME_144CYCLES, adc_channel);
        }
    }
    else /* ADC_Channel include in ADC_Channel_[0..9] */
    {
        /* Clear the old sample time */
        _ADC->SMPR2 &= ~ADC_SMPR2(ADC_SMPR2_SMP0, adc_channel);
        /* Set the new sample time */
        _ADC->SMPR2 |= ADC_SMPR2(ADC_SAMPLETIME_144CYCLES, adc_channel);
    }

    /* For Rank 1 to 6 */
    if(rank < 7)
    {
        /* Clear the old SQx bits for the selected rank */
        _ADC->SQR3 &= ~ADC_SQR3_RK(ADC_SQR3_SQ1, rank);
        /* Set the SQx bits for the selected rank */
        _ADC->SQR3 |= ADC_SQR3_RK(adc_channel, rank);
    }
    /* For Rank 7 to 12 */
    else if(rank < 13)
    {
        /* Clear the old SQx bits for the selected rank */
        _ADC->SQR2 &= ~ADC_SQR2_RK(ADC_SQR2_SQ7, rank);
        /* Set the SQx bits for the selected rank */
        _ADC->SQR2 |= ADC_SQR2_RK(adc_channel, rank);
    }
    /* For Rank 13 to 16 */
    else
    {
        /* Clear the old SQx bits for the selected rank */
        _ADC->SQR1 &= ~ADC_SQR1_RK(ADC_SQR1_SQ13, rank);
        /* Set the SQx bits for the selected rank */
        _ADC->SQR1 |= ADC_SQR1_RK(adc_channel, rank);
    }
}

static void config_adc_sequence(uint8_t seq_len)
{
    for(uint8_t rank = 0; rank < seq_len; ++rank)
    {
        // config adc channel
        uint8_t uchannel = multimeter.sequence[rank][0];
        uint8_t ichannel = multimeter.sequence[rank][1];
        config_adc_channel(ADC1, adc_in_by_channel[uchannel], rank + 1); // ADC RANK START FROM 1
        config_adc_channel(ADC2, adc_in_by_channel[ichannel], rank + 1);
    }

    ADC1->SQR1 |= (seq_len - 1) << ADC_SQR1_L_Pos;
    ADC2->SQR1 |= (seq_len - 1) << ADC_SQR1_L_Pos;
}

void multimeter_on_dma_interrupt(ADC_TypeDef* _ADC, DMA_Stream_TypeDef *dma_stream)
{
    if(ADC1 == _ADC)
    {
        if(DMA2->LISR & DMA_LISR_TEIF0)
        {
            DMA2->LIFCR |= DMA_LIFCR_CTEIF0;
        }
        // half transfer
        if(DMA2->LISR & DMA_LISR_HTIF0)
        {
            DMA2->LIFCR |= DMA_LIFCR_CHTIF0;
            multimeter.actual_data = multimeter.dma_buffer;
            multimeter.update_required = true;
        }
        // transfer complete
        if(DMA2->LISR & DMA_LISR_TCIF0)
        {
            DMA2->LIFCR |= DMA_LIFCR_CTCIF0;
            uint32_t offset = multimeter.data_len; // in hwords 
            multimeter.actual_data = &multimeter.dma_buffer[offset];
            multimeter.update_required = true;
        }
    }
}

static void dma_set_config(DMA_Stream_TypeDef *dma_stream, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength)
{
    /* Clear DBM bit */
    dma_stream->CR &= (uint32_t)(~DMA_SxCR_DBM);

    /* Configure DMA Stream data length */
    dma_stream->NDTR = DataLength;

    /* Peripheral to Memory */
    /* Configure DMA Stream source address */
    dma_stream->PAR = SrcAddress;

    /* Configure DMA Stream destination address */
    dma_stream->M0AR = DstAddress;
}

void dma_start(DMA_Stream_TypeDef* dma_stream, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength)
{
    uint32_t stream_number = (((uint32_t)dma_stream & 0xFFU) - 16U) / 24U;

    /* lookup table for necessary bitshift of flags within status registers */
    static const uint8_t flagBitshiftOffset[8U] = {0U, 6U, 16U, 22U, 0U, 6U, 16U, 22U};
    uint8_t stream_index = flagBitshiftOffset[stream_number];
    uint32_t stream_base;
    if(stream_number > 3U)
    {
    /* return pointer to HISR and HIFCR */
        stream_base = (((uint32_t)dma_stream & (uint32_t)(~0x3FFU)) + 4U);
    }else{
    /* return pointer to LISR and LIFCR */
        stream_base = ((uint32_t)dma_stream & (uint32_t)(~0x3FFU));
    }
    
    DMA_Base_Registers *regs = (DMA_Base_Registers *)(stream_base & (uint32_t)(~0x3FFU));
    /* Configure the source, destination address and the data length */
    dma_set_config(dma_stream, SrcAddress, DstAddress, DataLength);
    
    /* Clear all interrupt flags at correct offset within the register */
    regs->IFCR = 0x3FU << stream_index;
    
    /* Enable Common interrupts*/
    dma_stream->CR  |= DMA_SxCR_HTIE | DMA_SxCR_TCIE | DMA_SxCR_TEIE | DMA_SxCR_DMEIE;

    dma_stream->FCR |= 0x00000080U;
    
    /* Enable the Peripheral */
    dma_stream->CR |= DMA_SxCR_EN;
}

#define ADC_STAB_DELAY_US ((uint32_t) 3U)

static void enable_adc(ADC_TypeDef* _ADC)
{
    __IO uint32_t counter = 0;
    if((_ADC->CR2 & ADC_CR2_ADON) != ADC_CR2_ADON)
    {
        SET_BIT(_ADC->CR2, ADC_CR2_ADON);
        /* Delay for ADC stabilization time */
        /* Compute number of CPU cycles to wait for */
        counter = (ADC_STAB_DELAY_US * (SystemCoreClock / 1000000));
        while(counter != 0)
        {
            --counter;
        }
    }
}

static void config_multi_mode(void)
{
    // Dual mode, ADC1 and ADC2 working together
    ADC->CCR |= ADC_CCR_MULTI_1 | ADC_CCR_MULTI_2;
    // ADC->CCR |= ADC_CCR_TSVREFE;
    // ADC->CCR &= ~(ADC_CCR_TSVREFE);
    ADC->CCR |= ADC_CCR_VBATE;
    
    ADC->CCR |= ADC_CCR_DMA_1;

    // set ext trigger only for ADC1 // rising edge
    ADC1->CR2 |= ADC_CR2_EXTEN_0 | ADC_CR2_EXTSEL_2; // TIM5 TRGO
    // ADC2->CR2 |= ADC_CR2_EXTEN_0 | ADC_CR2_EXTSEL_2; // NO NEED, do not uncomment
}

void start_adc_dma(void)
{
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_ADC1EN);
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_ADC2EN);
    
    SET_BIT(RCC->APB1ENR, RCC_APB1ENR_DACEN); // ERRATA WORKARUND DO NOT DELETE !!! 

    config_multi_mode();

    // interrupts
    ADC1->CR1 |= ADC_CR1_OVRIE; // ONLY FOR ADC1

    // scan mode
    ADC1->CR1 |= ADC_CR1_SCAN;
    ADC2->CR1 |= ADC_CR1_SCAN;

    // end of conversion
    ADC1->CR2 &= ~ADC_CR2_EOCS; // generate update on every seq conversion end

    // continuous // NOT FOR OUR CASE
    // ADC1->CR2 |= ADC_CR2_CONT;
    // ADC2->CR2 |= ADC_CR2_CONT;

    enable_adc(ADC1);
    enable_adc(ADC2);

    if((ADC_CR2_ADON == (ADC1->CR2 & ADC_CR2_ADON)) 
        && (ADC_CR2_ADON == (ADC2->CR2 & ADC_CR2_ADON)))
    {
        CLEAR_BIT(ADC1->SR, ADC_SR_EOC | ADC_SR_OVR); // Clear regular group conversion flag and overrun flag
        CLEAR_BIT(ADC2->SR, ADC_SR_EOC | ADC_SR_OVR); // Clear regular group conversion flag and overrun flag
        SET_BIT(ADC1->CR2, ADC_CR2_DMA);
        SET_BIT(ADC1->CR2, ADC_CR2_DDS); // dma non-stop
        dma_start(DMA2_Stream0, (uint32_t)&ADC->CDR, (uint32_t)multimeter.dma_buffer, multimeter.data_len);
    }
    NVIC_SetPriority(ADC_IRQn, 6);
    NVIC_EnableIRQ(ADC_IRQn);
}
#endif

uint16_t multimeter_vbat(void)
{
    return multimeter.vbat;
}

uint16_t multimeter_output_status(void)
{
    return multimeter.output_enable;
}

uint16_t multimeter_output_ready(void)
{
    return multimeter.output_ready;
}

int multimeter_output_configure(uint8_t output_index, uint8_t channel1, uint8_t channel2)
{
    if(output_index >= MM_NUM_OUTPUTS)
        return -1;
    if((channel1 < 14) && (channel2 < 14))
    {
        multimeter.output_enable |= (1 << output_index);
        multimeter.output_config[output_index][0] = channel1;
        multimeter.output_config[output_index][1] = channel2;
    }
    return 0;
}

int8_t multimeter_add_to_sequence(uint8_t index, uint8_t chan1, uint8_t chan2)
{
    if(multimeter.seq_len >= MM_NUM_OUTPUTS)
        return -1;
    if(index > MM_NUM_OUTPUTS)
        return -1;

    uint8_t sequence_position = multimeter.seq_len;
    
    multimeter.channel_enable_flag |= 1 << chan1;
    multimeter.channel_enable_flag |= 1 << chan2;

    multimeter.sequence[sequence_position][0] = chan1;
#ifndef _UNITTEST_
    if(multimeter.device)
        device_write_data(multimeter.device, UCHANNEL0_REG + 9 * index, chan1);
#endif
    multimeter.sequence[sequence_position][1] = chan2; 
#ifndef _UNITTEST_
    if(multimeter.device)
        device_write_data(multimeter.device, ICHANNEL0_REG + 9 * index, chan2);
#endif
    multimeter.sequence[sequence_position][2] = index; 
    multimeter.seq_len += 1;

    return 0;
}

void multimeter_add_vbat_to_sequence(void)
{
    if(multimeter.seq_len > MM_NUM_OUTPUTS)
        return;

    uint8_t sequence_position = multimeter.seq_len;
    
    multimeter.sequence[sequence_position][0] = 14;
    multimeter.sequence[sequence_position][1] = 14;
    multimeter.sequence[sequence_position][2] = -1; 
    multimeter.seq_len += 1;
}

uint8_t multimeter_set_pair_channel(uint8_t output, uint8_t is_ichan, uint8_t value)
{
    if(is_ichan < 2)
        multimeter.output_config[output][is_ichan] = value; 
    return 0;
}


uint8_t multimeter_pair_channel(uint8_t output, uint8_t is_ichan)
{
    if(is_ichan < 2)
        return multimeter.output_config[output][is_ichan];
    return 0;
}

uint8_t multimeter_seq_length(void)
{
    return multimeter.seq_len;
}

void multimeter_update_configure(void)
{
#ifndef _UNITTEST_
    adc_timer_stop();
#endif
    multimeter.seq_len = 0;
    uint8_t output = 0;
    for(; output < MM_NUM_OUTPUTS; ++output)
    {
        if(multimeter.output_enable & (1 << output))
        {
            multimeter_add_to_sequence(output, 
                                        multimeter.output_config[output][0],
                                        multimeter.output_config[output][1]);
        }
    }
    multimeter_add_vbat_to_sequence();

    uint8_t sequence_position = multimeter.seq_len; // rank
    // clear rest pairs
    for(uint8_t pos = sequence_position; pos < MM_NUM_OUTPUTS; ++pos)
    {
        multimeter.sequence[pos][0] = 0xFF;
        multimeter.sequence[pos][1] = 0xFF;
        multimeter.sequence[pos][2] = 0xFF;
    }
    // 2 - double buff 
    // 2 - samples in 1 measure
    multimeter.data_len = 2 * MM_NUM_SAMPLES * multimeter.seq_len; // NDTR WORDS of 4 bytes (uint32_t)
#ifndef _UNITTEST_
    config_adc_sequence(multimeter.seq_len);
    
    start_adc_dma();
    adc_timer_init();
#endif
}

#ifndef _UNITTEST_
void adc_msp_init(void)
{
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_ADC1EN);
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_ADC2EN);

    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOCEN);

    // PC0     ------> ADC1_IN10
    // PC1     ------> ADC1_IN11
    // PC2     ------> ADC1_IN12
    // PC3     ------> ADC1_IN13
    // PA1     ------> ADC1_IN1
    // PA3     ------> ADC1_IN3
    // PA4     ------> ADC1_IN4
    // PA5     ------> ADC1_IN5
    // PA6     ------> ADC1_IN6
    // PA7     ------> ADC1_IN7
    // PC4     ------> ADC1_IN14
    // PC5     ------> ADC1_IN15
    // PB0     ------> ADC1_IN8
    // PB1     ------> ADC1_IN9 

    GPIOC->MODER |= GPIO_MODER_MODER0 | GPIO_MODER_MODER1 | GPIO_MODER_MODER2 | GPIO_MODER_MODER3 | GPIO_MODER_MODER4 | GPIO_MODER_MODER5;
    GPIOA->MODER |= GPIO_MODER_MODER1 | GPIO_MODER_MODER3 | GPIO_MODER_MODER4 | GPIO_MODER_MODER5 | GPIO_MODER_MODER6 | GPIO_MODER_MODER7;
    GPIOB->MODER |= GPIO_MODER_MODER0 | GPIO_MODER_MODER1;

    // for ADC1, ADC2
    // DMA2_Stream0 channel0
    DMA2_Stream0->CR &=  ~(DMA_SxCR_EN /*| DMA_SxCR_CHSEL*/);
    DMA2_Stream0->CR |= DMA_SxCR_MINC |     // DMA_MINC_ENABLE
                        DMA_SxCR_PSIZE_1 |  // DMA_PDATAALIGN_WORD
                        DMA_SxCR_MSIZE_1 |  // DMA_MDATAALIGN_WORD
                        DMA_SxCR_CIRC |     // Circular mode 
                        DMA_SxCR_PL_1;      // Priority level: High

                        // DMA_FIFOMODE_DISABLE
                        // DMA_PINC_DISABLE;
                        // DMA_PERIPH_TO_MEMORY

}
#endif

void multimeter_set_adc_zero_level(uint16_t val)
{
    multimeter.adc_zero_level = val;
}

uint16_t multimeter_adc_zero_level(void)
{
    return multimeter.adc_zero_level;
}

uint16_t multimeter_delta(uint8_t ch)
{
    if(ch < MM_NUM_CHANNELS)
        return multimeter.delta[ch];
    return 0;
}

void multimeter_set_delta(uint8_t ch, uint16_t value)
{
    if(ch < MM_NUM_CHANNELS)
        multimeter.delta[ch] = value;
}

void multimeter_reset(void)
{
    memset(multimeter.output_config, 0, sizeof(multimeter.output_config));
    memset(multimeter.sequence, 0, sizeof(multimeter.sequence));
    memset(multimeter.S, 0, sizeof(multimeter.S));
    memset(multimeter.P, 0, sizeof(multimeter.P));
    memset(multimeter.Q, 0, sizeof(multimeter.Q));
    memset(multimeter.cosFI, 0, sizeof(multimeter.cosFI));
    memset(multimeter.delta, 0, sizeof(multimeter.delta));
    memset(multimeter.dma_buffer, 0, sizeof(multimeter.dma_buffer));
}

void multimeter_init(modbus_device_t* device)
{
#ifndef _UNITTEST_
    adc_msp_init();
#endif
    multimeter_reset();

    multimeter.actual_data = multimeter.dma_buffer;
    multimeter.output_enable = 0;
    multimeter.device = device;
    multimeter.adc_zero_level = MM_ADC_DEFAULT_ZERO_LEVEL;
    multimeter.period = MM_NUM_SAMPLES;
    multimeter_set_rate(DEFAULT_RATE);
}

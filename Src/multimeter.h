#ifndef __MULTIMETER__
#define __MULTIMETER__ 1

#include <stdbool.h>
#ifndef _UNITTEST_
 	  #include "stm32f7xx.h"
#else
	  #include <stdint.h>
#endif

#include "device_data.h"

#define MM_TIMER_RATE     10000
#define MM_NUM_OUTPUTS    6   // channel pairs
#define MM_NUM_CHANNELS  	15  // channel

#ifdef PHASE_LEN_CHANGER
  // #define MM_NUM_SAMPLES    333 // in half buffer
#else
#define MM_NUM_SAMPLES    200 // in half buffer
#endif
#define MM_ADC_DEFAULT_ZERO_LEVEL	0x800

#define MM_BUFFER_LEN  (2 * 2 * (MM_NUM_OUTPUTS + 1) * MM_NUM_SAMPLES) // HWORDS
// 2 - double buffer
// 2 - hwords in sample ADC1 + ADC2

typedef struct
{
    uint8_t seq_len;

    uint8_t output_config[MM_NUM_OUTPUTS][2];
    uint8_t sequence[MM_NUM_OUTPUTS + 1][3]; // index, channel1, channel2 I, + 1seq for vref
    uint16_t dma_buffer[MM_BUFFER_LEN];
    uint16_t* actual_data;
    uint32_t period;
    uint32_t data_len; // DMA2 S0 NDTR - количество слов на весь буффер дма. данные копируются из ADC->CDR

    bool 	update_required;
    bool 	adc_ready;
    bool  vbat_ready;

    uint16_t adc_zero_level;
    int16_t delta[MM_NUM_CHANNELS]; // for test on bad generator
    uint32_t process_time_diff; // debug
    modbus_device_t* device;

    uint16_t channel_enable_flag; // 14 channels
    uint16_t channel_ready_flag;
    uint8_t  output_enable; // 7 channels
    uint8_t  output_ready; // 7 channels

    float 	uavg[MM_NUM_CHANNELS];
    float 	urms[MM_NUM_CHANNELS];

    float 	P[MM_NUM_OUTPUTS]; // active power
    float 	Q[MM_NUM_OUTPUTS]; // reactive
    float 	S[MM_NUM_OUTPUTS]; // full
    float 	cosFI[MM_NUM_OUTPUTS];
    uint16_t vbat;
}multimeter_t;

#ifndef _UNITTEST_
#define ADC_CHANNEL_0           ((uint32_t)0x00000000U)
#define ADC_CHANNEL_1           ((uint32_t)ADC_CR1_AWDCH_0)
#define ADC_CHANNEL_2           ((uint32_t)ADC_CR1_AWDCH_1)
#define ADC_CHANNEL_3           ((uint32_t)(ADC_CR1_AWDCH_1 | ADC_CR1_AWDCH_0))
#define ADC_CHANNEL_4           ((uint32_t)ADC_CR1_AWDCH_2)
#define ADC_CHANNEL_5           ((uint32_t)(ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_0))
#define ADC_CHANNEL_6           ((uint32_t)(ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_1))
#define ADC_CHANNEL_7           ((uint32_t)(ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_1 | ADC_CR1_AWDCH_0))
#define ADC_CHANNEL_8           ((uint32_t)ADC_CR1_AWDCH_3)
#define ADC_CHANNEL_9           ((uint32_t)(ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_0))
#define ADC_CHANNEL_10          ((uint32_t)(ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_1))
#define ADC_CHANNEL_11          ((uint32_t)(ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_1 | ADC_CR1_AWDCH_0))
#define ADC_CHANNEL_12          ((uint32_t)(ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_2))
#define ADC_CHANNEL_13          ((uint32_t)(ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_0))
#define ADC_CHANNEL_14          ((uint32_t)(ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_1))
#define ADC_CHANNEL_15          ((uint32_t)(ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_1 | ADC_CR1_AWDCH_0))
#define ADC_CHANNEL_16          ((uint32_t)ADC_CR1_AWDCH_4)
#define ADC_CHANNEL_17          ((uint32_t)(ADC_CR1_AWDCH_4 | ADC_CR1_AWDCH_0))
#define ADC_CHANNEL_18          ((uint32_t)(ADC_CR1_AWDCH_4 | ADC_CR1_AWDCH_1))

#define ADC_INTERNAL_NONE                  0x80000000U
#define ADC_CHANNEL_VREFINT     ((uint32_t)ADC_CHANNEL_17)
#define ADC_CHANNEL_VBAT        ((uint32_t)ADC_CHANNEL_18)
#define ADC_CHANNEL_TEMPSENSOR  ((uint32_t)(ADC_CHANNEL_18 | 0x10000000U))

#define ADC_SAMPLETIME_3CYCLES    ((uint32_t)0x00000000U)
#define ADC_SAMPLETIME_15CYCLES   ((uint32_t)ADC_SMPR1_SMP10_0)
#define ADC_SAMPLETIME_28CYCLES   ((uint32_t)ADC_SMPR1_SMP10_1)
#define ADC_SAMPLETIME_56CYCLES   ((uint32_t)(ADC_SMPR1_SMP10_1 | ADC_SMPR1_SMP10_0))
#define ADC_SAMPLETIME_84CYCLES   ((uint32_t)ADC_SMPR1_SMP10_2)
#define ADC_SAMPLETIME_112CYCLES  ((uint32_t)(ADC_SMPR1_SMP10_2 | ADC_SMPR1_SMP10_0))
#define ADC_SAMPLETIME_144CYCLES  ((uint32_t)(ADC_SMPR1_SMP10_2 | ADC_SMPR1_SMP10_1))
#define ADC_SAMPLETIME_480CYCLES  ((uint32_t)ADC_SMPR1_SMP10)
#endif
/**
  * @brief  Set ADC Regular channel sequence length.
  * @param  _NbrOfConversion_ Regular channel sequence length. 
  * @retval None
  */
#define ADC_SQR1(_NbrOfConversion_) (((_NbrOfConversion_) - (uint8_t)1) << 20)

/**
  * @brief  Set the ADC's sample time for channel numbers between 10 and 18.
  * @param  _SAMPLETIME_ Sample time parameter.
  * @param  _CHANNELNB_ Channel number.  
  * @retval None
  */
#define ADC_SMPR1(_SAMPLETIME_, _CHANNELNB_) ((_SAMPLETIME_) << (3 * (((uint32_t)((uint16_t)(_CHANNELNB_))) - 10)))

/**
  * @brief  Set the ADC's sample time for channel numbers between 0 and 9.
  * @param  _SAMPLETIME_ Sample time parameter.
  * @param  _CHANNELNB_ Channel number.  
  * @retval None
  */
#define ADC_SMPR2(_SAMPLETIME_, _CHANNELNB_) ((_SAMPLETIME_) << (3 * ((uint32_t)((uint16_t)(_CHANNELNB_)))))

/**
  * @brief  Set the selected regular channel rank for rank between 1 and 6.
  * @param  _CHANNELNB_ Channel number.
  * @param  _RANKNB_ Rank number.    
  * @retval None
  */
#define ADC_SQR3_RK(_CHANNELNB_, _RANKNB_) (((uint32_t)((uint16_t)(_CHANNELNB_))) << (5 * ((_RANKNB_) - 1)))

/**
  * @brief  Set the selected regular channel rank for rank between 7 and 12.
  * @param  _CHANNELNB_ Channel number.
  * @param  _RANKNB_ Rank number.    
  * @retval None
  */
#define ADC_SQR2_RK(_CHANNELNB_, _RANKNB_) (((uint32_t)((uint16_t)(_CHANNELNB_))) << (5 * ((_RANKNB_) - 7)))

/**
  * @brief  Set the selected regular channel rank for rank between 13 and 16.
  * @param  _CHANNELNB_ Channel number.
  * @param  _RANKNB_ Rank number.    
  * @retval None
  */
#define ADC_SQR1_RK(_CHANNELNB_, _RANKNB_) (((uint32_t)((uint16_t)(_CHANNELNB_))) << (5 * ((_RANKNB_) - 13)))

uint16_t*  multimeter_buffer(void);
void multimeter_set_actual_buffer(uint16_t* buffer);


int32_t multimeter_P(uint8_t rank);
float   multimeter_S(uint8_t rank);
float   multimeter_Q(uint8_t rank);
float 	multimeter_cosFI(uint8_t rank);

void 	multimeter_on_adc_error(void);
void 	multimeter_on_timer_interrupt(void);

void 	   multimeter_set_period(uint16_t period); // in samples
uint16_t multimeter_period(void);
void     multimeter_set_rate(uint32_t rate);
uint16_t multimeter_rate(void);

void 	multimeter_update(void);
void  multimeter_init(modbus_device_t* device);
uint8_t  multimeter_seq_length(void);

bool 	multimeter_is_ready(void);
void 	multimeter_clear_ready_flag(void);

uint16_t multimeter_channels_enabled(void);
uint16_t multimeter_channels_ready(void);

int8_t multimeter_add_to_sequence(uint8_t output_index, uint8_t chan1, uint8_t chan2);
void 	 multimeter_update_configure(void);

int8_t multimeter_get_next_active_channel(uint8_t from);
void   multimeter_on_adc_interrupt(void);
#ifndef _UNITTEST_
void   multimeter_on_dma_interrupt(ADC_TypeDef* _ADC, DMA_Stream_TypeDef *dma_stream);
#endif
float  multimeter_uavg(uint8_t channel);
float  multimeter_urms(uint8_t channel);
uint16_t multimeter_vbat(void);
uint16_t multimeter_output_status(void);
uint16_t multimeter_output_ready(void);

int multimeter_output_configure(uint8_t output_index, uint8_t channel1, uint8_t channel2);
uint8_t multimeter_pair_channel(uint8_t pair, uint8_t is_ichan);
uint8_t multimeter_set_pair_channel(uint8_t pair, uint8_t is_ichan, uint8_t value);

uint16_t multimeter_adc_zero_level(void);
void multimeter_set_adc_zero_level(uint16_t val);
uint16_t multimeter_delta(uint8_t ch);
void multimeter_set_delta(uint8_t ch, uint16_t value);
void multimeter_reset(void);

#endif

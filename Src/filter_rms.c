
#include "filter_rms.h"
#include <string.h>
#include <math.h>
#include <stdio.h>

void rms_filter_push(rms_t* rms, int16_t value)
{
	rms->buffer[rms->write_ptr] = value * value;
	rms->write_ptr = (rms->write_ptr < FILTER_RMS_ORDER - 1) ? rms->write_ptr + 1 : 0;
}

float rms_get(rms_t* rms, uint32_t datalen)
{
	uint32_t output = 0;
	if(datalen && (datalen <= FILTER_RMS_ORDER))
	{
		for(uint32_t i = 0; i < datalen; i++)
		{
			printf("%d: %u\n", i, rms->buffer[i]);
			output += rms->buffer[i];
		}
		output /= datalen;
	}

	return sqrtf(output);
}

void rms_init(rms_t* rms)
{
	rms->write_ptr = 0;
	memset(rms->buffer, 0, sizeof(rms->buffer));
}

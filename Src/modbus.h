#ifndef __MBUS_H__
#define __MBUS_H__
#ifndef UNITTEST
#include "stm32f7xx.h"
#endif
#include "modbus_port.h"

struct
{
    uint16_t program_crc; // for self check
    uint16_t ext_mem_crc;
    uint32_t change_time;
    uint32_t total_rq;   // счетчик запросов
    uint32_t handled_rq; // счетчик обработанных запросов
    uint32_t err_crc_rq; // счетчик запросов с ошибкой CRC
    uint32_t err_param_rq; // счетчик запросов с некорректными параметрами
    uint32_t err_send;  // счетчик ошибок при отправке
}modbus;

enum{
    FUNC_CODE_ERR = 1,  // 01 — Принятый код функции не может быть обработан.
    DATA_ADDR_ERR,      // 02 — Адрес данных, указанный в запросе, недоступен.
    REQUEST_ERR,        // 03 — Значение, содержащееся в поле данных запроса, является недопустимой величиной.
    FATAL_ERR,          // 04 — Невосстанавливаемая ошибка имела место, пока ведомое устройство пыталось выполнить затребованное действие.
    IN_PROGRESS,        // 05 — Ведомое устройство приняло запрос и обрабатывает его, но это требует много времени. Этот ответ предохраняет ведущее устройство от генерации ошибки тайм-аута.
    DEVICE_BUSY,        // 06 — Ведомое устройство занято обработкой команды. Ведущее устройство должно повторить сообщение позже, когда ведомое освободится.
    CANNOT_DOIT,        // 07 — Ведомое устройство не может выполнить программную функцию, заданную в запросе. Этот код возвращается для неуспешного программного запроса, использующего функции с номерами 13 или 14. Ведущее устройство должно запросить диагностическую информацию или информацию об ошибках от ведомого.
    READ_PARITY_ERR,    // 08 — Ведомое устройство при чтении расширенной памяти обнаружило ошибку контроля четности.
}modbus_error;

void modbus_send_exception(void);
void modbus_send_response(void);
void modbus_on_message(void);

void modbus_check_trasmition(modbus_port_t* mbport, size_t len);

int8_t modbus_on_rtu(modbus_port_t* mbport, uint8_t* message, uint16_t size);
int8_t modbus_f3(modbus_port_t* mbport,  uint8_t* message);
int8_t modbus_response_echo(modbus_port_t* mbport, uint8_t* message);
int8_t modbus_response_error(modbus_port_t* mbport, uint8_t* message, uint8_t error_code);

#endif // __MBUS_H__

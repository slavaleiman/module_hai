#include "main.h"
#include "stm32f7xx.h"
#include <stdio.h>
#include "multimeter.h"
#include "device_data.h"
#include "circular_buffer.h"
#include "modbus_port.h"

void system_clock_config(void);
static void GPIO_init(void);
static void DMA_init(void);

modbus_device_t device;
modbus_port_t mbport3;
uint32_t _process_time = 0;

#define ART_ACCLERATOR_ENABLE 0U
#define PREFETCH_ENABLE       0U
#define TICK_INT_PRIORITY     ((uint32_t)15U)
__IO uint32_t uwTick;

int8_t init_tick(uint32_t tick_priority)
{
    /* Configure the SysTick to have interrupt in 1ms time basis*/
    if (SysTick_Config(SystemCoreClock / (1000U / uwTickFreq)) > 0U)
    {
        return -1;
    }

    /* Configure the SysTick IRQ priority */
    if (tick_priority < (1UL << __NVIC_PRIO_BITS))
    {
        uint32_t prioritygroup = NVIC_GetPriorityGrouping();
        NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(prioritygroup, 5, 0));
        NVIC_EnableIRQ(SysTick_IRQn);
    }
    else
    {
        return -1;
    }

    /* Return function status */
    return 0;
}

void preinit(void)
{
    NVIC_SetPriorityGrouping((uint32_t)0x00000003U); // 4 bits for pre-emption priority, 0 bits for subpriority */
    SystemCoreClockUpdate();

    /* Use systick as time base source and configure 1ms tick (default clock after Reset is HSI) */
    init_tick(TICK_INT_PRIORITY);

    /* Init the low level hardware */
    SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_SYSCFGEN);
    uint32_t prioritygroup = 0x00;
    uint32_t PreemptPriority = 15;
    uint32_t SubPriority = 0;

    prioritygroup = NVIC_GetPriorityGrouping();

    NVIC_SetPriority(PendSV_IRQn, NVIC_EncodePriority(prioritygroup, PreemptPriority, SubPriority));

    // enable FPU interrupt
    NVIC_SetPriority(FPU_IRQn, NVIC_EncodePriority(prioritygroup, PreemptPriority, SubPriority));
    NVIC_EnableIRQ(FPU_IRQn);
}

int main(void)
{
    system_clock_config();
    preinit();
    GPIO_init();
    DMA_init();

    device.addr = MODBUS_DEFAULT_ADDR;
    device.read_data = device_read_data;
    device.write_data = device_write_data;
    CIRCULAR_BUFFER_DEF(uart3_cbuffer, 128 * 8);

#define DE_GPIO_PORT NULL 
#define DE_GPIO_PIN  0

    mbport_init(&mbport3,
            USART3,
            DMA1_Stream1,
            DMA_SxCR_CHSEL_2,
            DMA1_Stream3,
            DMA_SxCR_CHSEL_2,
            115200,
            &uart3_cbuffer,
            &device,
            DE_GPIO_PORT,
            1 << DE_GPIO_PIN // (1 << pin_number)
            );

    multimeter_init(&device);

    multimeter_output_configure(0, 0, 5);
    multimeter_output_configure(1, 1, 8);
    multimeter_output_configure(2, 2, 9);
    multimeter_output_configure(3, 3, 10);
    multimeter_output_configure(4, 4, 11);
    multimeter_output_configure(5, 5, 12);

    multimeter_update_configure();
#if 0

    // переинициализация
    multimeter_reset();
    multimeter_output_configure(0, 0, 5);
    multimeter_output_configure(1, 1, 8);

    multimeter_update_configure();
#endif


#if 0 
    char msg[250];
    int32_t P;
    int32_t Q, S, cosFi, avg1, avg2, rms1, rms2;
#endif

// DEBUG
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT->LAR = 0xC5ACCE55;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
//
    while (1)
    {
        uint32_t t1 = DWT->CYCCNT; // debug

        multimeter_update();

        mbport_process(&mbport3);

        uint32_t t2 = DWT->CYCCNT;
        _process_time = t2 - t1; // debug

#if 0
        if(multimeter_is_ready())
        {
            multimeter_clear_ready_flag();

            P = multimeter_P(0);
            S = multimeter_S(0);
            Q = multimeter_Q(0);
            cosFi = 1000 * multimeter_cosFI(0);
            avg1 = multimeter_uavg(0);
            avg2 = multimeter_uavg(7);
            rms1 = multimeter_urms(0);
            rms2 = multimeter_urms(7);
            uint8_t size = sprintf(msg, "P:%08ld Q:%08ld S:%08ld cosfi:%08ld uavg1:%08ld uavg2:%08ld rms1:%08ld rms2:%08ld\n", 
                P, Q, S, cosFi, avg1, avg2, rms1, rms2);
            UART_Transmit_DMA(USART3, (uint8_t*)msg, size);
        }
#endif
    }
}

#define PLL_M 4
#define PLL_N 216
#define PLL_P 2
#define PLL_Q 2
#define HSE_STARTUP_TIMEOUT    100U

void system_clock_config(void)
{
    __IO uint32_t StartUpCounter = 0, HSEStatus = 0;

    /* Enable HSE */
    RCC->CR |= ((uint32_t)RCC_CR_HSEON);
    /* Wait till HSE is ready and if Time out is reached exit */
    do
    {
        HSEStatus = RCC->CR & RCC_CR_HSERDY;
        StartUpCounter++;
    } while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

    if ((RCC->CR & RCC_CR_HSERDY) != RESET)
    {
        HSEStatus = (uint32_t)0x01;
    }
    else
    {
        HSEStatus = (uint32_t)0x00;
    }

    if (HSEStatus == (uint32_t)0x01)
    {
        /* Select regulator voltage output Scale 1 mode, System frequency up to 168 MHz */
        RCC->APB1ENR |= RCC_APB1ENR_PWREN;
        PWR->CR1 |= PWR_CR1_VOS;

        /* HCLK = SYSCLK / 1*/
        RCC->CFGR |= RCC_CFGR_HPRE_DIV1;

        /* PCLK2 = HCLK / 2*/
        RCC->CFGR |= RCC_CFGR_PPRE2_DIV2;

        /* PCLK1 = HCLK / 4*/
        RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;

        /* Configure the main PLL */
        RCC->PLLCFGR = PLL_M | (PLL_N << 6) | (((PLL_P >> 1) -1) << 16) |
        (RCC_PLLCFGR_PLLSRC_HSE) | (PLL_Q << 24);

        /* Enable the main PLL */
        RCC->CR |= RCC_CR_PLLON;

        /* Wait till the main PLL is ready */
        while((RCC->CR & RCC_CR_PLLRDY) == 0)
        {
        }

        /* Configure Flash prefetch, Instruction cache, Data cache and wait state */
        FLASH->ACR = FLASH_ACR_ARTEN | FLASH_ACR_PRFTEN | FLASH_ACR_LATENCY_7WS;

        /* Select the main PLL as system clock source */
        RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
        RCC->CFGR |= RCC_CFGR_SW_PLL;

        /* Wait till the main PLL is used as system clock source */
        while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_PLL)
        {
        }
    }
    else
    { /* If HSE fails to start-up, the application will have wrong clock
        configuration. User can add here some code to deal with this error */
    }
    // update SystemCoreClock variable
    SystemCoreClockUpdate();
}


static void DMA_init(void) 
{
    /* DMA controller clock enable */
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_DMA2EN); 
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_DMA1EN);

    uint32_t prioritygroup = NVIC_GetPriorityGrouping();
    // USART3 RX
    NVIC_SetPriority(DMA1_Stream1_IRQn, NVIC_EncodePriority(prioritygroup, 5, 0));
    NVIC_EnableIRQ(DMA1_Stream1_IRQn);
    // USART3 TX
    NVIC_SetPriority(DMA1_Stream3_IRQn, NVIC_EncodePriority(prioritygroup, 5, 0));
    NVIC_EnableIRQ(DMA1_Stream3_IRQn);
    // ADC1,2
    NVIC_SetPriority(DMA2_Stream0_IRQn, NVIC_EncodePriority(prioritygroup, 5, 0));
    NVIC_EnableIRQ(DMA2_Stream0_IRQn);
}

static void GPIO_init(void)
{
    /* GPIO Ports Clock Enable */
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOHEN);
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOCEN);
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);
}

void usart3_gpio_init(modbus_port_t* port)
{
    SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART3EN);
    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);
    // PD8     ------> USART3_TX
    // PD9     ------> USART3_RX 
    GPIOD->MODER |= GPIO_MODER_MODER8_1 | GPIO_MODER_MODER9_1;
    GPIOD->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR8 | GPIO_OSPEEDR_OSPEEDR9;
    GPIOD->AFR[1] |= (7 << GPIO_AFRH_AFRH0_Pos) | (7 << GPIO_AFRH_AFRH1_Pos);
}

void Error_Handler(void)
{
    while(1);
}

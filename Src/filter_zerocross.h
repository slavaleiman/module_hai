
#include <stdint.h>
#include <stdbool.h>

#define ZCF_WINDOW_SIZE 500 // TODO set size < phase size in samples
#define ZCF_ZERO_VALUE  0

typedef struct 
{
	uint32_t counter;
	uint32_t phase_size;
	int16_t value;
}zcf_t;

bool zcf_filter(zcf_t* zcf, int16_t value);
uint32_t zcf_phase_size(zcf_t* zcf);
void zcf_init(zcf_t* zcf);

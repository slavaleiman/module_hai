
// адрес устройства         1 byte
// номер функции            1 byte
// Адрес регистра           2 byte
// Количество флагов        2 byte
// Количество byte данных   1 byte
// Данные                   ...
// CRC                      2 byte

#include "modbus.h"
// #include "modbus_proxy.h"
#include "string.h"
#include "CRC.h"
// #include "udelay.h"
#include "errors.h"
#include "modbus_port.h"

#define DEVICE_DEFAULT_ADDR     1

void modbus_inc_crc_err_rq(void)
{
    ++modbus.err_crc_rq;
}

void modbus_inc_handled_rq(void)
{
    ++modbus.handled_rq;
}

void modbus_inc_total_rq(void)
{
    ++modbus.total_rq;
}

void modbus_inc_send_err(void)
{
    ++modbus.err_send;
}

int8_t modbus_response_error(modbus_port_t* mbport, uint8_t* message, uint8_t error_code)
{
    mbport->tx_buffer[0] = message[0];
    mbport->tx_buffer[1] = message[1] | 0x80;
    mbport->tx_buffer[2] = error_code;
    const uint16_t crc = CRC16(&mbport->tx_buffer[0], 3);
    mbport->tx_buffer[3] = crc & 0xFF;
    mbport->tx_buffer[4] = crc >> 8;
    mbport->tx_size = 5;
    return 0;
}

int8_t modbus_response_echo(modbus_port_t* mbport, uint8_t* message)
{
    uint16_t _crc = message[7] << 8 | message[6];
    mbport->tx_buffer[0] = message[0];
    mbport->tx_buffer[1] = 0x8;
    mbport->tx_buffer[2] = 0;
    mbport->tx_buffer[3] = 0;
    mbport->tx_buffer[4] = message[4];
    mbport->tx_buffer[5] = message[5];
    const uint16_t calc_crc = CRC16(&mbport->tx_buffer[0], 6);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return -1;
    }
    mbport->tx_buffer[6] = calc_crc & 0xFF;
    mbport->tx_buffer[7] = calc_crc >> 8;
    mbport->tx_size = 8;
    return 0;
}

static int8_t get_holding_reg(modbus_port_t* mbport, uint8_t* message)
{
    const uint16_t start_addr = message[2] << 8 | message[3];
    const uint16_t req_regs_num = message[4] << 8 | message[5];
    uint16_t _crc = message[6] | message[7] << 8;
    uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return 0;
    }
    mbport->tx_buffer[0] = message[0];
    mbport->tx_buffer[1] = 3;

    uint8_t count_reg = 0;
    int8_t ret = 0;
    // if(mbport->holding_mutex != NULL)
    // {
        // if(xSemaphoreTake(mbport->holding_mutex, (TickType_t)10) == pdTRUE) 
        // {
            while(count_reg < req_regs_num)
            {
                uint16_t reg_value;
                if(mbport->device && mbport->device->read_data)
                {
                    ret = mbport->device->read_data(mbport->device, start_addr + count_reg, &reg_value);
                    if(ret < 0)
                        break;
                    mbport->tx_buffer[3 + count_reg * 2] = reg_value >> 8;
                    mbport->tx_buffer[3 + count_reg * 2 + 1] = reg_value & 0xFF;
                    ++count_reg;
                }else{
                    ret = DATA_ADDR_ERR;
                    ERROR(MODBUS_PORT_CANNOT_READ);
                    break;
                }
            }
            // xSemaphoreGive(mbport->holding_mutex);
        // }else{
            // ERROR(MODBUS_HOLDING_REG_LOCKED);
        // }
    // }

    if(!count_reg)
        return ret;

    mbport->tx_buffer[2] = count_reg * 2;
    const uint16_t crc = CRC16(&mbport->tx_buffer[0], count_reg * 2 + 3);
    // fill tx buffer but not send
    mbport->tx_buffer[3 + count_reg * 2] = crc & 0xFF;
    mbport->tx_buffer[3 + count_reg * 2 + 1] = crc >> 8;
    mbport->tx_size = count_reg * 2 + 5;
    return 0;
}

// Read Holding Registers
int8_t modbus_f3(modbus_port_t* mbport,  uint8_t* message)
{
    uint8_t errcode = get_holding_reg(mbport, message);
    if(errcode)
        modbus_response_error(mbport, message, errcode);
    else
        modbus_inc_handled_rq();
    return 0;
}

static int8_t write_single_reg(modbus_port_t* mbport, uint8_t* message)
{
    const uint16_t reg_addr    = message[2] << 8 | message[3];
    const uint16_t write_value = message[4] << 8 | message[5];
    uint16_t _crc = message[6] | message[7] << 8;
    uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return 0;
    }
    int8_t ret = -1;
    // if(mbport->holding_mutex != NULL)
    // {
        // if(xSemaphoreTake(mbport->holding_mutex, (TickType_t)10) == pdTRUE) 
        // {
            if(mbport->device && mbport->device->write_data)
                ret = mbport->device->write_data(mbport->device, reg_addr, write_value);
            else{
                ret = -1;
                ERROR(MODBUS_PORT_CANNOT_WRITE);
            }
            // xSemaphoreGive(mbport->holding_mutex);
        // }else{
            // ERROR(MODBUS_HOLDING_REG_LOCKED);
        // }
    // }
    if(ret)
        return ret;
    mbport->tx_buffer[0] = message[0];
    mbport->tx_buffer[1] = 6;
    mbport->tx_buffer[2] = message[2];
    mbport->tx_buffer[3] = message[3];
    mbport->tx_buffer[4] = message[4];
    mbport->tx_buffer[5] = message[5];
    mbport->tx_buffer[6] = message[6];
    mbport->tx_buffer[7] = message[7];
    mbport->tx_size = 8;
    return 0;
}

int8_t modbus_f6(modbus_port_t* mbport, uint8_t* message)
{
    int8_t errcode = write_single_reg(mbport, message);
    if(errcode)
        modbus_response_error(mbport, message, errcode); 
    else
        modbus_inc_handled_rq();
    return 0;
}

static int8_t write_multiple_reg(modbus_port_t* port, uint8_t* message, uint16_t size)
{
    const uint16_t start_addr = message[2] << 8 | message[3];
    const uint16_t regs_to_write = message[4] << 8 | message[5];
    const uint8_t bytes_to_write = message[6];

    uint16_t _crc = message[7 + bytes_to_write] | message[8 + bytes_to_write] << 8;
    uint16_t calc_crc = CRC16(&message[0], 7 + bytes_to_write);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return 0;
    }

    port->tx_buffer[0] = message[0];
    port->tx_buffer[1] = 16;

    uint8_t count_reg = 0;
    // if(port->holding_mutex != NULL)
    // {
        // if(xSemaphoreTake(port->holding_mutex, (TickType_t)10) == pdTRUE) 
        // {
            while(count_reg < regs_to_write)
            {
                const uint8_t index = 7 + count_reg * 2;
                if(index + 1 > size - 2)
                {
                    break;
                }
                uint16_t reg_value = message[index] << 8 | message[index + 1];
                if(port->device && port->device->write_data)
                {

                    int8_t ret = port->device->write_data(port->device, start_addr + count_reg * 2, reg_value);
                    if(ret)
                        break;
                }else{
                    break;
                }
                // если записался хоть один регистр - то
                // при ошибке записи в следующий, просто выходим
                // и отправляем количество успешно записанных регистров
                ++count_reg;
            }
            // xSemaphoreGive(port->holding_mutex);
        // }else{
            // ERROR(MODBUS_HOLDING_REG_LOCKED);
        // }
    // }    

    if(!count_reg) // если не записалось ниодного - ошибка в запросе
        return REQUEST_ERR;
    port->tx_buffer[2] = message[2];
    port->tx_buffer[3] = message[3];
    port->tx_buffer[4] = count_reg >> 8;
    port->tx_buffer[5] = count_reg & 0xFF;

    const uint16_t crc = CRC16(&port->tx_buffer[0], 6);
    port->tx_buffer[6] = crc & 0xFF;
    port->tx_buffer[7] = crc >> 8;
    port->tx_size = 8;
    return 0;
}

int8_t modbus_f16(modbus_port_t* mbport, uint8_t* message, uint8_t size)
{
    int8_t errcode = write_multiple_reg(mbport, message, size);
    if(errcode)
        modbus_response_error(mbport, message, errcode);
    else
        modbus_inc_handled_rq();
    return 0;
}

int8_t modbus_on_rtu(modbus_port_t* port, uint8_t* message, uint16_t size)
{
    const uint8_t addr = message[0];
    if(addr != port->device->addr)
        return 1;
    modbus_inc_total_rq();

    if(size < 8)
        return 1;

    const uint8_t func_num = message[1];
    switch(func_num)
    {
        case 3: // чтение значений из нескольких регистров хранения (Read Holding Registers).
            return modbus_f3(port, message);
        case 6: // запись значения в один регистр хранения
            return modbus_f6(port, message);
        case 8: // Диагностика
            return modbus_response_echo(port, message);
        case 16: // запись значений в несколько регистров хранения (Preset Multiple Registers)
            return modbus_f16(port, message, size);
        default:
            modbus_response_error(port, message, FUNC_CODE_ERR);
    }

    return 0;
}

// WARNING MAY BE NEED MUTEX HERE!
void modbus_check_trasmition(modbus_port_t* mbport, size_t len)
{
    for(uint8_t i = 0; i < len; ++i)
    {
        if(mbport->dma_rx_buffer[i] ^ mbport->tx_buffer[i])
        {
            modbus_inc_send_err();
            return;
        }
    }
}

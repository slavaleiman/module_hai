
#include <stdint.h>

/* Choose filter order */
#define FILTER_SMA_ORDER 24

/**
  * @brief Simple Moving Average (SMA) filter.
  * @note Before use define filter order.
  * @param[in] Input raw (unfiltered) value.
  * @retval Return filtered data.
  */
typedef struct 
{
	int16_t buffer[FILTER_SMA_ORDER];
	uint16_t write_ptr;
}sma_t;

int16_t sma_filter(sma_t* sma, int16_t value);
void sma_init(sma_t* sma);

#ifndef __device_data_h__
#define __device_data_h__ 1

#define REGS_DATA_OFFSET 23 // TODO set normal offset

#define MODBUS_DEFAULT_ADDR 60 
#define DEFAULT_RATE 		5000 // * 100
#define APB1_PRESC_BITS		((RCC->CFGR & RCC_CFGR_PPRE1) >> RCC_CFGR_PPRE1_Pos)
#define APB1_BUS_DIV		(2 << (APB1_PRESC_BITS & 0x3))
#define TIM_BUS_FREQ		(SystemCoreClock / APB1_BUS_DIV)
// HERE MUST BE P Q S cosFI
enum{
	RATE_MEASURE_CHANNEL_REG = 0,
	RATE_REG = 1,
	DEFAULT_RATE_REG = 2,

	ENABLED_CHANNELS = 3, // read only
	CHANNELS_READY = 4, // read only

	RMS0_REG = 5,
	AVG0_REG,
	RMS1_REG,
	AVG1_REG,
	RMS2_REG,
	AVG2_REG, // 10
	RMS3_REG,
	AVG3_REG,
	RMS4_REG,
	AVG4_REG,
	RMS5_REG, //
	AVG5_REG,
	RMS6_REG,
	AVG6_REG,
	RMS7_REG,
	AVG7_REG, // 20
	RMS8_REG,
	AVG8_REG,
	RMS9_REG,
	AVG9_REG,
	RMS10_REG, //
	AVG10_REG,
	RMS11_REG,
	AVG11_REG,
	RMS12_REG,
	AVG12_REG, // 30
	RMS13_REG,
	AVG13_REG,
	
	UBAT_REG = 33,

	MEASURE_CHANNEL_PAIR_MASK = 34,
	CHANNEL_PAIR_READY_MASK = 35,

	UCHANNEL0_REG = 36,
	ICHANNEL0_REG = 37,
	P0_H_REG,
	P0_L_REG,
	Q0_H_REG,
	Q0_L_REG,
	S0_H_REG,
	S0_L_REG,
	COS0_REG,

	UCHANNEL1_REG = 45,
	ICHANNEL1_REG,
	P1_H_REG,
	P1_L_REG,
	Q1_H_REG,
	Q1_L_REG,
	S1_H_REG,
	S1_L_REG,
	COS1_REG,

	UCHANNEL2_REG = 54,
	ICHANNEL2_REG,
	P2_H_REG,
	P2_L_REG,
	Q2_H_REG,
	Q2_L_REG,
	S2_H_REG,
	S2_L_REG,
	COS2_REG,

	UCHANNEL3_REG = 63,
	ICHANNEL3_REG,
	P3_H_REG,
	P3_L_REG,
	Q3_H_REG,
	Q3_L_REG,
	S3_H_REG,
	S3_L_REG,
	COS3_REG,

	UCHANNEL4_REG = 72,
	ICHANNEL4_REG,
	P4_H_REG,
	P4_L_REG,
	Q4_H_REG,
	Q4_L_REG,
	S4_H_REG,
	S4_L_REG,
	COS4_REG,

	UCHANNEL5_REG = 81,
	ICHANNEL5_REG,
	P5_H_REG,
	P5_L_REG,
	Q5_H_REG,
	Q5_L_REG,
	S5_H_REG,
	S5_L_REG,
	COS5_REG,
	
	ADC_ZERO_LEVEL_REG = 90,

	CHANNEL_DELTA_0 = 91,
	CHANNEL_DELTA_1,
	CHANNEL_DELTA_2,
	CHANNEL_DELTA_3,
	CHANNEL_DELTA_4,
	CHANNEL_DELTA_5 = 96,
	CHANNEL_DELTA_6,
	CHANNEL_DELTA_7,
	CHANNEL_DELTA_8,
	CHANNEL_DELTA_9 = 100,
	CHANNEL_DELTA_10,
	CHANNEL_DELTA_11,
	CHANNEL_DELTA_12,
	CHANNEL_DELTA_13,

	UPDATE_CONFIG_REG = 105,
}MM_REGS;

// enum{
// CHANM_REG = 1, // 1 Канал, в котором измеряется частота 1 0 0 15
// _REG, // 2 Значение частоты, Гц*100 1 0 3000 7000
// _REG, // 3 Значение частоты по умолчанию, Гц * 100 1 5000 3000 7000
// _REG, // 4 Маска запрошенных каналов измерения U 1 0 0 0x3FFF
// _REG, // 5 Маска каналов с вычисленными результатами измерений U 1 0 0 0x3FFF
// URMS0_REG, // 6 Цифровой код N, соответствующий Urms канала 0 1 0 0 2047
// UAVG0_REG, // 7 Цифровой код N, соответствующий Uavg канала 0 1 0 -2048 2047
// URMS0_REG, // 8 Цифровой код N, соответствующий Urms канала 1 1 0 0 2047
// UAVG0_REG, // 9 Цифровой код N, соответствующий Uavg канала 1 1 0 -2048 2047
// URMS0_REG, // 10 Цифровой код N, соответствующий Urms канала 2 1 0 0 2047
// UAVG0_REG, // 11 Цифровой код N, соответствующий Uavg канала 2 1 0 -2048 2047
// URMS0_REG, // 12 Цифровой код N, соответствующий Urms канала 3 1 0 0 2047
// UAVG0_REG, // 13 Цифровой код N, соответствующий Uavg канала 3 1 0 -2048 2047
// URMS0_REG, // 14 Цифровой код N, соответствующий Urms канала 4 1 0 0 2047
// UAVG0_REG, // 15 Цифровой код N, соответствующий Uavg канала 4 1 0 -2048 2047
// URMS0_REG, // 16 Цифровой код N, соответствующий Urms канала 5 1 0 0 2047
// UAVG0_REG, // 17 Цифровой код N, соответствующий Uavg канала 5 1 0 -2048 2047
// URMS0_REG, // 18 Цифровой код N, соответствующий Urms канала 6 1 0 0 2047
// UAVG0_REG, // 19 Цифровой код N, соответствующий Uavg канала 6 1 0 -2048 2047
// URMS0_REG, // 20 Цифровой код N, соответствующий Urms канала 7 1 0 0 2047
// UAVG0_REG, // 21 Цифровой код N, соответствующий Uavg канала 7 1 0 -2048 2047
// URMS0_REG, // 22 Цифровой код N, соответствующий Urms канала 8 1 0 0 2047
// UAVG0_REG, // 23 Цифровой код N, соответствующий Uavg канала 8 1 0 -2048 2047
// URMS0_REG, // 24 Цифровой код N, соответствующий Urms канала 9 1 0 0 2047
// UAVG0_REG, // 25 Цифровой код N, соответствующий Uavg канала 9 1 0 -2048 2047
// URMS0_REG, // 26 Цифровой код N, соответствующий Urms канала 10 1 0 0 2047
// UAVG0_REG, // 27 Цифровой код N, соответствующий Uavg канала 10 1 0 -2048 2047
// URMS0_REG, // 28 Цифровой код N, соответствующий Urms канала 11 1 0 0 2047
// UAVG0_REG, // 29 Цифровой код N, соответствующий Uavg канала 11 1 0 -2048 2047
// URMS0_REG, // 30 Цифровой код N, соответствующий Urms канала 12 1 0 0 2047
// UAVG0_REG, // 31 Цифровой код N, соответствующий Uavg канала 12 1 0 -2048 2047
// URMS0_REG, // 32 Цифровой код N, соответствующий Urms канала 13 1 0 0 2047
// UAVG0_REG, // 33 Цифровой код N, соответствующий Uavg канала 13 1 0 -2048 2047
// UBAT_REG, // 34 Цифровой код N, соответствующий напряжении бвтареи 1 0 -2048 2047
// REQ_MASK_REG, // 35 Маска запрошенных каналов измерения P, Q, S, PF 1 0 0 0x3F
// RESULT_MASK_REG, // 36 Маска каналов с вычисленными результатами измерений P, Q, S, PF 1 0 0 0x3F

// INPUT_CHANNEL_U_REG, // 37 Номер канала U 1 0 0 13
// INPUT_CHANNEL_I_REG, // 38 Номер канала I 1 0 0 13
// P0_REG, // 39 Активная мощность P0 2 0 0 4190209
// Q0_REG, // 40 Реактивная мощность Q0 2 0 -4190210 4190209
// S0_REG, // 41 Полная мощность S0 2 0 0 4190209
// PF0_REG, // 42 Коэффициент мощности PF0 * 10000 1 0 0 9999

// INPUT_CHANNEL_U_REG, // 43 Номер канала U 1 0 0 13
// INPUT_CHANNEL_I_REG, // 44 Номер канала I 1 0 0 13
// P1_REG, // 45 Активная мощность P1 2 0 0 4190209
// _REG, // 46 Реактивная мощность Q1 2 0 -4190210 4190209
// _REG, // 47 Полная мощность S1 2 0 0 4190209
// _REG, // 48 Коэффициент мощности PF1 * 10000 1 0 0 9999
// _REG, // 49 Номер канала U 1 0 0 
// _REG, // 50 Номер канала I 1 0 0 13
// _REG, // 51 Активная мощность P2 2 0 0 4190209
// _REG, // 52 Реактивная мощность Q2 2 0 -4190210 4190209
// _REG, // 53 Полная мощность S2 2 0 0 4190209
// _REG, // 54 Коэффициент мощности PF2 * 10000 1 0 0 9999
// _REG, // 55 Номер канала U 1 0 0 13
// _REG, // 56 Номер канала I 1 0 0 13
// _REG, // 57 Активная мощность P3 2 0 0 4190209
// _REG, // 58 Реактивная мощность Q3 2 0 -4190210 4190209
// _REG, // 59 Полная мощность S3 2 0 0 4190209
// _REG, // 60 Коэффициент мощности PF3 * 10000 1 0 0 9999
// _REG, // 61 Номер канала U 1 0 0 13
// _REG, // 62 Номер канала I 1 0 0 13
// _REG, // 63 Активная мощность P4 2 0 0 4190209
// _REG, // 64 Реактивная мощность Q4 2 0 -4190210 4190209
// _REG, // 65 Полная мощность S4 2 0 0 4190209
// _REG, // 66 Коэффициент мощности PF4 * 10000 1 0 0 9999
// _REG, // 67 Номер канала U 1 0 0 13
// _REG, // 68 Номер канала I 1 0 0 13
// _REG, // 69 Активная мощность P5 2 0 0 4190209
// _REG, // 70 Реактивная мощность Q5 2 0 -4190210 4190209
// _REG, // 71 Полная мощность S5 2 0 0 4190209
// _REG, // 72 Коэффициент мощности PF5 * 10000 1 0 0 9999
// }DEVICE_REGS;

typedef struct modbus_device_t
{
	int8_t (*read_data)(struct modbus_device_t*, uint16_t reg, uint16_t* value);
	int8_t (*write_data)(struct modbus_device_t*, uint16_t reg, uint16_t value);
    uint16_t addr;
}modbus_device_t;

int8_t device_read_data(modbus_device_t*, uint16_t reg, uint16_t* value);
int8_t device_write_data(modbus_device_t*, uint16_t reg, uint16_t value);

#endif

#include <string.h>
#include "circular_buffer.h"
#include "errors.h"
#include <stdint.h>

int cbuffer_push(circular_buffer_t *cbuff, uint8_t *item, size_t size)
{
    if(cbuff->count + size > cbuff->maxlen)
    {
        return -1;
    }
    
    if(cbuff->head + size < cbuff->buffer_end)
        memcpy(cbuff->head, item, size);
    else{
        int skip = cbuff->buffer_end - cbuff->head;
        memcpy(cbuff->head, item, skip);
        memcpy(cbuff->buffer, &item[skip], size - skip);
    }

    cbuff->head = (char*)cbuff->head + size;
    if(cbuff->head >= cbuff->buffer_end)
        cbuff->head -= cbuff->maxlen;

    cbuff->count += size;
    return 0;
}

int cbuffer_push_message(circular_buffer_t *cbuff, uint8_t* data, uint16_t len)
{
    message_t mess;
    mess.data = cbuff->head + sizeof(message_t);
    if(mess.data > cbuff->buffer_end)
        mess.data -= cbuff->maxlen;
    mess.len = len;

    if(cbuffer_push(cbuff, (uint8_t*)&mess, sizeof(message_t))) 
    {
        return -1;
    }
    if(cbuffer_push(cbuff, data, len))
    {
        return -1;
    }
    return 0;
}

int cbuffer_pop(circular_buffer_t *cbuff, uint8_t *item, size_t size)
{
    if(!cbuff->count || (cbuff->count - size < 0))
    {
        return -1;
    }

    uint32_t skip = 0;
    if(cbuff->tail + size <= cbuff->buffer_end)
    {
        memcpy(item, cbuff->tail, size);
        // TODO check ret value
    }else{
        skip = cbuff->buffer_end - cbuff->tail;
        memcpy(item, cbuff->tail, skip);
        // TODO check ret value
        memcpy(&item[skip], cbuff->buffer, size - skip);
        // TODO check ret value
    }

    cbuff->tail = (char*)cbuff->tail + size;
    if(cbuff->tail >= cbuff->buffer_end)
        cbuff->tail -= cbuff->maxlen;
    cbuff->count -= size;
    return 0;
}

// return length of readed message
int cbuffer_pop_message(circular_buffer_t *cbuff, uint8_t* data)
{
    message_t in_mess;
    // если circular buffer не пуст
    if(!cbuffer_pop(cbuff, (uint8_t*)&in_mess, sizeof(message_t)))
    {
        if(in_mess.len > 255)
        {
            // TODO FLUSH CBUFFER ON ERROR
            return -1;
        }
        if(cbuff->tail != in_mess.data)
        {
            // TODO FLUSH CBUFFER ON ERROR
            return -1;
        }
        if(!cbuffer_pop(cbuff, data, in_mess.len))
        {
            return in_mess.len;
        }
    }
    return 0;
}

// when comes second part of messaage
// its tail = data 
int cbuffer_check_data(circular_buffer_t *cbuff, uint8_t* data)
{
    if(cbuff->tail != data)
        return -1;
    return 0;
}

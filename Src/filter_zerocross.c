
// input must be filtered by sma

#include "filter_zerocross.h"
#include <string.h>
#include <stdio.h>

bool zcf_filter(zcf_t* zcf, int16_t value)
{
	bool ret = false;
	if(zcf->counter > ZCF_WINDOW_SIZE)
	{
	    if(zcf->value)
	    {
	        if((zcf->value < ZCF_ZERO_VALUE) && (value >= ZCF_ZERO_VALUE) && (zcf->value < value))
	        {
				zcf->phase_size = zcf->counter;
        		zcf->counter = 0;
            	ret = true;
        	}
    	}
	}
	if(zcf->counter < 0xFFFFFFFF)
		++zcf->counter;

	zcf->value = value;
	return ret;
}

uint32_t zcf_phase_size(zcf_t* zcf)
{
	return zcf->phase_size;
}

void zcf_init(zcf_t* zcf)
{
	zcf->phase_size = 0;
	zcf->counter = 0;
}


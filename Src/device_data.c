
#include "stm32f7xx.h"
#include "multimeter.h"
#include "modbus.h"
#include "device_data.h"

int8_t device_read_data(modbus_device_t* device, uint16_t reg, uint16_t* value)
{
	switch(reg)
	{
		case RATE_MEASURE_CHANNEL_REG: *value = 0;
			break;
		case RATE_REG: *value = multimeter_rate();
			break;
		case DEFAULT_RATE_REG: *value = DEFAULT_RATE;
			break;
		case ENABLED_CHANNELS: *value = multimeter_channels_enabled();
			break; // read only
		case CHANNELS_READY: *value = multimeter_channels_ready();
			break; // read only

		case RMS0_REG: *value = (int16_t)multimeter_urms(0); break;
		case AVG0_REG: *value = (int16_t)multimeter_uavg(0); break;
		case RMS1_REG: *value = (int16_t)multimeter_urms(1); break;
		case AVG1_REG: *value = (int16_t)multimeter_uavg(1); break;
		case RMS2_REG: *value = (int16_t)multimeter_urms(2); break;
		case AVG2_REG: *value = (int16_t)multimeter_uavg(2); break;
		case RMS3_REG: *value = (int16_t)multimeter_urms(3); break;
		case AVG3_REG: *value = (int16_t)multimeter_uavg(3); break;
		case RMS4_REG: *value = (int16_t)multimeter_urms(4); break;
		case AVG4_REG: *value = (int16_t)multimeter_uavg(4); break;
		case RMS5_REG: *value = (int16_t)multimeter_urms(5); break;
		case AVG5_REG: *value = (int16_t)multimeter_uavg(5); break;
		case RMS6_REG: *value = (int16_t)multimeter_urms(6); break;
		case AVG6_REG: *value = (int16_t)multimeter_uavg(6); break;
		case RMS7_REG: *value = (int16_t)multimeter_urms(7); break;
		case AVG7_REG: *value = (int16_t)multimeter_uavg(7); break;
		case RMS8_REG: *value = (int16_t)multimeter_urms(8); break;
		case AVG8_REG: *value = (int16_t)multimeter_uavg(8); break;
		case RMS9_REG: *value = (int16_t)multimeter_urms(9); break;
		case AVG9_REG: *value = (int16_t)multimeter_uavg(9); break;
		case RMS10_REG: *value = (int16_t)multimeter_urms(10); break;
		case AVG10_REG: *value = (int16_t)multimeter_uavg(10); break;
		case RMS11_REG: *value = (int16_t)multimeter_urms(11); break;
		case AVG11_REG: *value = (int16_t)multimeter_uavg(11); break;
		case RMS12_REG: *value = (int16_t)multimeter_urms(12); break;
		case AVG12_REG: *value = (int16_t)multimeter_uavg(12); break;
		case RMS13_REG: *value = (int16_t)multimeter_urms(13); break;
		case AVG13_REG: *value = (int16_t)multimeter_uavg(13); break;
		
		case UBAT_REG: *value = multimeter_vbat(); break;

		case MEASURE_CHANNEL_PAIR_MASK: *value = multimeter_output_status();
			break;
		case CHANNEL_PAIR_READY_MASK: *value = multimeter_output_ready();
			break;

		case UCHANNEL0_REG:
			*value = multimeter_pair_channel(0, 0); break;
		case ICHANNEL0_REG:
			*value = multimeter_pair_channel(0, 1); break;
		case P0_H_REG:
			*value = multimeter_P(0) >> 16; break;
		case P0_L_REG:
			*value = multimeter_P(0) & 0xFFFF; break;
		case Q0_H_REG:
			*value = (uint32_t)multimeter_Q(0) >> 16; break;
		case Q0_L_REG:
			*value = (uint32_t)multimeter_Q(0) & 0xFFFF; break;
		case S0_H_REG:
			*value = (uint32_t)multimeter_S(0) >> 16; break;
		case S0_L_REG:
			*value = (uint32_t)multimeter_S(0) & 0xFFFF; break;
		case COS0_REG:
			*value = (int16_t)(multimeter_cosFI(0) * 10000); break;

		case UCHANNEL1_REG:
			*value = multimeter_pair_channel(1, 0); break;
		case ICHANNEL1_REG:
			*value = multimeter_pair_channel(1, 1); break;
		case P1_H_REG:
			*value = multimeter_P(1) >> 16; break;
		case P1_L_REG:
			*value = multimeter_P(1) & 0xFFFF; break;
		case Q1_H_REG:
			*value = (uint32_t)multimeter_Q(1) >> 16; break;
		case Q1_L_REG:
			*value = (uint32_t)multimeter_Q(1) & 0xFFFF; break;
		case S1_H_REG:
			*value = (uint32_t)multimeter_S(1) >> 16; break;
		case S1_L_REG:
			*value = (uint32_t)multimeter_S(1) & 0xFFFF; break;
		case COS1_REG:
			*value = (int16_t)(multimeter_cosFI(1) * 10000); break;

		case UCHANNEL2_REG:
			*value = multimeter_pair_channel(2, 0); break;
		case ICHANNEL2_REG:
			*value = multimeter_pair_channel(2, 1); break;
		case P2_H_REG:
			*value = multimeter_P(2) >> 16; break;
		case P2_L_REG:
			*value = multimeter_P(2) & 0xFFFF; break;
		case Q2_H_REG:
			*value = (uint32_t)multimeter_Q(2) >> 16; break;
		case Q2_L_REG:
			*value = (uint32_t)multimeter_Q(2) & 0xFFFF; break;
		case S2_H_REG:
			*value = (uint32_t)multimeter_S(2) >> 16; break;
		case S2_L_REG:
			*value = (uint32_t)multimeter_S(2) & 0xFFFF; break;
		case COS2_REG:
			*value = (int16_t)(multimeter_cosFI(2) * 10000); break;

		case UCHANNEL3_REG:
			*value = multimeter_pair_channel(3, 0); break;
		case ICHANNEL3_REG:
			*value = multimeter_pair_channel(3, 1); break;
		case P3_H_REG:
			*value = multimeter_P(3) >> 16; break;
		case P3_L_REG:
			*value = multimeter_P(3) & 0xFFFF; break;
		case Q3_H_REG:
			*value = (uint32_t)multimeter_Q(3) >> 16; break;
		case Q3_L_REG:
			*value = (uint32_t)multimeter_Q(3) & 0xFFFF; break;
		case S3_H_REG:
			*value = (uint32_t)multimeter_S(3) >> 16; break;
		case S3_L_REG:
			*value = (uint32_t)multimeter_S(3) & 0xFFFF; break;
		case COS3_REG:
			*value = (int16_t)(multimeter_cosFI(3) * 10000); break;

		case UCHANNEL4_REG:
			*value = multimeter_pair_channel(4, 0); break;
		case ICHANNEL4_REG:
			*value = multimeter_pair_channel(4, 1); break;
		case P4_H_REG:
			*value = multimeter_P(4) >> 16; break;
		case P4_L_REG:
			*value = multimeter_P(4) & 0xFFFF; break;
		case Q4_H_REG:
			*value = (uint32_t)multimeter_Q(4) >> 16; break;
		case Q4_L_REG:
			*value = (uint32_t)multimeter_Q(4) & 0xFFFF; break;
		case S4_H_REG:
			*value = (uint32_t)multimeter_S(4) >> 16; break;
		case S4_L_REG:
			*value = (uint32_t)multimeter_S(4) & 0xFFFF; break;
		case COS4_REG:
			*value = (int16_t)(multimeter_cosFI(4) * 10000); break;

		case UCHANNEL5_REG:
			*value = multimeter_pair_channel(5, 0); break;
		case ICHANNEL5_REG:
			*value = multimeter_pair_channel(5, 1); break;
		case P5_H_REG:
			*value = multimeter_P(5) >> 16; break;
		case P5_L_REG:
			*value = multimeter_P(5) & 0xFFFF; break;
		case Q5_H_REG:
			*value = (uint32_t)multimeter_Q(5) >> 16; break;
		case Q5_L_REG:
			*value = (uint32_t)multimeter_Q(5) & 0xFFFF; break;
		case S5_H_REG:
			*value = (uint32_t)multimeter_S(5) >> 16; break;
		case S5_L_REG:
			*value = (uint32_t)multimeter_S(5) & 0xFFFF; break;
		case COS5_REG:
			*value = (int16_t)(multimeter_cosFI(5) * 10000); break;

		case ADC_ZERO_LEVEL_REG:
			*value = multimeter_adc_zero_level(); break;

		case CHANNEL_DELTA_0:
			multimeter_delta(0); break;
		case CHANNEL_DELTA_1:
			multimeter_delta(1); break;
		case CHANNEL_DELTA_2:
			multimeter_delta(2); break;
		case CHANNEL_DELTA_3:
			multimeter_delta(3); break;
		case CHANNEL_DELTA_4:
			multimeter_delta(4); break;
		case CHANNEL_DELTA_5:
			multimeter_delta(5); break;
		case CHANNEL_DELTA_6:
			multimeter_delta(6); break;
		case CHANNEL_DELTA_7:
			multimeter_delta(7); break;
		case CHANNEL_DELTA_8:
			multimeter_delta(8); break;
		case CHANNEL_DELTA_9:
			multimeter_delta(9); break;
		case CHANNEL_DELTA_10:
			multimeter_delta(10); break;
		case CHANNEL_DELTA_11:
			multimeter_delta(11); break;
		case CHANNEL_DELTA_12:
			multimeter_delta(12); break;
		case CHANNEL_DELTA_13:
			multimeter_delta(13); break;

		default:
			return DATA_ADDR_ERR;
	}
	return 0;
}

int8_t device_write_data(modbus_device_t* device, uint16_t reg, uint16_t value)
{
	switch(reg)
	{
		// case RATE_MEASURE_CHANNEL:
		// case MEASURE_CHANNEL_PAIR_MASK:
		case RATE_REG:
			multimeter_set_rate(value);
			break;

		case UCHANNEL0_REG:
			multimeter_set_pair_channel(0, 0, value); break;
		case ICHANNEL0_REG:
			multimeter_set_pair_channel(0, 1, value); break;
		case UCHANNEL1_REG:
			multimeter_set_pair_channel(1, 0, value); break;
		case ICHANNEL1_REG:
			multimeter_set_pair_channel(1, 1, value); break;
		
		case UCHANNEL2_REG:
			multimeter_set_pair_channel(2, 0, value); break;
		case ICHANNEL2_REG:
			multimeter_set_pair_channel(2, 1, value); break;
		
		case UCHANNEL3_REG:
			multimeter_set_pair_channel(3, 0, value); break;
		case ICHANNEL3_REG:
			multimeter_set_pair_channel(3, 1, value); break;
		
		case UCHANNEL4_REG:
			multimeter_set_pair_channel(4, 0, value); break;
		case ICHANNEL4_REG:
			multimeter_set_pair_channel(4, 1, value); break;
		
		case UCHANNEL5_REG:
			multimeter_set_pair_channel(5, 0, value); break;
		case ICHANNEL5_REG:
			multimeter_set_pair_channel(5, 1, value); break;

		case UPDATE_CONFIG_REG:
		    multimeter_update_configure(); break;

		case ADC_ZERO_LEVEL_REG:
			multimeter_set_adc_zero_level(value); break;

		case CHANNEL_DELTA_0:
			multimeter_set_delta(0, value); break;
		case CHANNEL_DELTA_1:
			multimeter_set_delta(1, value); break;
		case CHANNEL_DELTA_2:
			multimeter_set_delta(2, value); break;
		case CHANNEL_DELTA_3:
			multimeter_set_delta(3, value); break;
		case CHANNEL_DELTA_4:
			multimeter_set_delta(4, value); break;
		case CHANNEL_DELTA_5:
			multimeter_set_delta(5, value); break;
		case CHANNEL_DELTA_6:
			multimeter_set_delta(6, value); break;
		case CHANNEL_DELTA_7:
			multimeter_set_delta(7, value); break;
		case CHANNEL_DELTA_8:
			multimeter_set_delta(8, value); break;
		case CHANNEL_DELTA_9:
			multimeter_set_delta(9, value); break;
		case CHANNEL_DELTA_10:
			multimeter_set_delta(10, value); break;
		case CHANNEL_DELTA_11:
			multimeter_set_delta(11, value); break;
		case CHANNEL_DELTA_12:
			multimeter_set_delta(12, value); break;
		case CHANNEL_DELTA_13:
			multimeter_set_delta(13, value); break;

		// case MODBUS_ADDR:
		// 	device->addr = value;
		// 	break;

		default:
			return DATA_ADDR_ERR;
	}
	return 0;
}

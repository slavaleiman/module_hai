#include "main.h"
#include "stm32f7xx_it.h"
#include "multimeter.h"
#include "modbus_port.h"
#include "errors.h"

extern modbus_port_t mbport3;

void NMI_Handler(void)
{
}

void HardFault_Handler(void)
{
    while (1)
    {
    }
}

void MemManage_Handler(void)
{
    while (1)
    {
    }
}

void BusFault_Handler(void)
{
    while (1)
    {
    }
}

void UsageFault_Handler(void)
{
    while (1)
    {
    }
}

void DebugMon_Handler(void)
{
}

void SysTick_Handler(void)
{
    uwTick += uwTickFreq;
}

void DMA1_Stream1_IRQHandler(void)
{
    usart_on_dma_interrupt(DMA1_Stream1);
}

void DMA1_Stream3_IRQHandler(void)
{
    usart_on_dma_interrupt(DMA1_Stream3);
}

void DMA2_Stream0_IRQHandler(void)
{
    multimeter_on_dma_interrupt(ADC1, DMA2_Stream0);
}

void DMA2_Stream1_IRQHandler(void)
{
    // multimeter_on_dma_interrupt(ADC3, DMA2_Stream3);
}

void DMA2_Stream3_IRQHandler(void)
{
    // multimeter_on_dma_interrupt(ADC2, DMA2_Stream1);
}

// void TIM6_DAC_IRQHandler(void)
void TIM5_IRQHandler(void)
{
    multimeter_on_timer_interrupt();
}

void ADC_IRQHandler(void)
{
    multimeter_on_adc_interrupt();
}

void USART3_IRQHandler(void)
{
    usart_irq_handler(&mbport3);
}

void FPU_IRQHandler(void)
{
    errors_on_error(FPU_ERROR);
}

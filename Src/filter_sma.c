
#include "filter_sma.h"
#include <string.h>

/**
  * @brief Simple Moving Average (SMA) filter.
  * @note Before use define filter order.
  * @param[in] Input raw (unfiltered) value.
  * @retval Return filtered data.
  */
int16_t sma_filter(sma_t* sma, int16_t value)
{
	sma->buffer[sma->write_ptr] = value;
	sma->write_ptr = (sma->write_ptr < FILTER_SMA_ORDER - 1) ? sma->write_ptr + 1 : 0;
	int32_t output = 0;
	for(uint8_t i = 0; i < FILTER_SMA_ORDER; i++)
	{
		output += sma->buffer[i];
	}
	output /= FILTER_SMA_ORDER;
	return (int16_t)output;
}

void sma_init(sma_t* sma)
{
	memset(sma->buffer, 0, sizeof(sma->buffer));
}

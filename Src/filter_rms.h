
#include <stdint.h>

#define FILTER_RMS_ORDER 300

typedef struct
{
	uint32_t buffer[FILTER_RMS_ORDER];
	uint16_t write_ptr;
}rms_t;

void rms_filter_push(rms_t* rms, int16_t value);
float rms_get(rms_t* rms, uint32_t datalen);
void rms_init(rms_t* rms);

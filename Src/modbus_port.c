#include "main.h"
#include "modbus_port.h"
#ifndef UNITTEST
#include <stdbool.h>
#endif
#include "modbus.h"
#include "errors.h"

void usart_transmit_dma(USART_TypeDef *USART, uint8_t* msg, uint16_t size);

void mbport_transmit(modbus_port_t* mbport)
{
#ifndef UNITTEST
    usart_transmit_dma(mbport->usart, mbport->tx_buffer, mbport->tx_size);
    mbport->is_sending = true;
#endif
}

uint32_t _port_process_time = 0;

void mbport_process(modbus_port_t* mbport)
{
    uint32_t t1 = DWT->CYCCNT; // debug
    int16_t len = cbuffer_pop_message(mbport->cbuffer, (uint8_t*)&mbport->rx_buffer);
    if(len > 0)
    {

        int ret = modbus_on_rtu(mbport, mbport->rx_buffer, len);
        if(!ret)
        {
            if(mbport->tx_size)
                mbport_transmit(mbport);
        }else{
            errors_on_error(ret);
            return;
        }
        uint32_t t2 = DWT->CYCCNT;
        _port_process_time = t2 - t1; // debug
    }
    if(len < 0)
    {
        errors_on_error(CBUFFER_POP_ERROR);
    }
}

// debug
uint32_t push_time_diff = 0;

void usart_irq_handler(modbus_port_t *port)
{

//     QueueHandle_t rx_queue              = port->RxQueue;
//     osSemaphoreId rx_semaphore_handle   = port->UsartRxSemaphoreHandle;
    (void)port->usart->RDR;
    if(port->usart->ISR & USART_ISR_IDLE)
    {

        port->usart->ICR |= USART_ISR_IDLE | USART_ISR_RXNE;
        size_t len = DMA_RX_BUFFER_SIZE - port->dma_input_stream->NDTR;
        if(len)
        {
#ifdef CHECK_TX
            if(port->is_sending == true) // отправленное сообщение
            {
                port->is_sending = false;
                port->dma_input_stream->CR &= ~DMA_SxCR_EN;
                modbus_check_trasmition(port, len);
                memset(port->dma_rx_buffer, 0, len);
                port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer; /* Set memory address for DMA again */
                port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                port->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
            }
            else
                port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer; /* Set memory address for DMA again */
                port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                port->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
            }
            else
#endif
            {
                uint32_t t1 = DWT->CYCCNT;

                port->dma_input_stream->CR &= ~DMA_SxCR_EN;        

                if(cbuffer_push_message(port->cbuffer, port->dma_rx_buffer, len) == -1)
                    errors_on_error(CBUFFER_PUSH_ERROR);
                memset(port->dma_rx_buffer, 0, len);

                port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer; /* Set memory address for DMA again */
                port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                port->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
                
                // debug
                uint32_t t2 = DWT->CYCCNT;
                
                push_time_diff = t2 - t1;
            }
        }
    }
    if(port->usart->ISR & USART_ISR_ORE)
    {
        // port->usart->SR &= ~USART_SR_ORE;
        port->usart->ICR |= USART_ICR_ORECF;
        (void)port->usart->RDR;
        port->usart->CR1 &= ~USART_CR1_UE;
        port->usart->CR3 |= USART_CR3_EIE | USART_CR3_DMAR;
        port->usart->CR1 |= USART_CR1_UE;
        // reinit_dma
        memset(port->dma_rx_buffer, 0, DMA_RX_BUFFER_SIZE);
        port->dma_input_stream->CR &= ~DMA_SxCR_EN;
        port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer; /* Set memory address for DMA again */
        port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
        port->dma_input_stream->CR |= DMA_SxCR_TCIE | DMA_SxCR_TEIE;
        port->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
        errors_on_error(RS485_OVERRUN_ERROR); // without led
    }
}

#ifndef UNITTEST
void mbport_gpio_init(modbus_port_t *port)
{

    // __HAL_RCC_GPIOA_CLK_ENABLE();
    // GPIO_InitTypeDef GPIO_InitStruct = {0};
    // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    // GPIO_InitStruct.Pull = GPIO_NOPULL;
    // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    // GPIO_InitStruct.Pin = GPIO_PIN_15;
    // GPIO_InitStruct.Alternate = GPIO_AF4_USART4;
    // HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

extern modbus_port_t mbport3;

void usart_gpio_init(modbus_port_t* port)
{
	if(port == &mbport3)
	{
	    SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART3EN);
	    SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);
	    // PD8     ------> USART3_TX
	    // PD9     ------> USART3_RX 
	    GPIOD->MODER |= GPIO_MODER_MODER8_1 | GPIO_MODER_MODER9_1;
	    GPIOD->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR8 | GPIO_OSPEEDR_OSPEEDR9;
	    GPIOD->AFR[1] |= (7 << GPIO_AFRH_AFRH0_Pos) | (7 << GPIO_AFRH_AFRH1_Pos);
	}
}

void mbport_de_reset(modbus_port_t* port)
{
	if(port->de_port && port->de_pin)
	{
	    port->de_port->BSRR = (uint32_t)port->de_pin << 16; // bit reset
	}
}

static void dma_config(modbus_port_t* port)
{
    port->dma_input_stream->CR  |= DMA_SxCR_TCIE | DMA_SxCR_TEIE | DMA_SxCR_DMEIE;
    port->dma_input_stream->FCR |= 0x00000080U;

    port->dma_input_stream->CR &= (uint32_t)(~DMA_SxCR_DBM);

    /* Configure DMA Stream data length */
    port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;
    /* Peripheral to Memory */
    /* Configure DMA Stream source address */
    port->dma_input_stream->PAR = (uint32_t)&port->usart->RDR;
    /* Configure DMA Stream destination address */
    port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer;

    port->dma_input_stream->CR |= port->dma_input_stream_channel 
                                | DMA_SxCR_MINC 
                                | DMA_SxCR_CIRC; 

    // may be calc baseregister DMA

    /* USART3_TX Init */
    port->dma_output_stream->CR |= port->dma_input_stream_channel
                                | DMA_SxCR_MINC;

    port->dma_output_stream->FCR |= 0x00000080U; 
}

void usart_init(modbus_port_t* port)
{
    usart_gpio_init(port);

    port->usart->CR1 |= USART_CR1_TE | USART_CR1_RE | USART_CR1_IDLEIE | USART_CR1_OVER8;

    uint32_t usartdiv = (SystemCoreClock + port->baudrate / 2) / port->baudrate;
    uint16_t brrtemp = (uint16_t)(usartdiv & 0xFFF0U);
    brrtemp |= (uint16_t)((usartdiv & (uint16_t)0x000FU) >> 1U);
    port->usart->BRR = brrtemp;
    port->usart->CR3 |= USART_CR3_DMAT | USART_CR3_DMAR;
    port->usart->CR1 |= USART_CR1_UE;

    mbport_de_reset(port);

    port->is_sending = false;

    dma_config(port);

    uint32_t prioritygroup = NVIC_GetPriorityGrouping();

    NVIC_SetPriority(USART3_IRQn, NVIC_EncodePriority(prioritygroup, 5, 0));
    NVIC_EnableIRQ(USART3_IRQn);
}

static void dma_transmit(DMA_Stream_TypeDef* dma_stream, uint32_t mem_address, uint32_t periph_address, uint32_t DataLength)
{
    uint32_t stream_number = (((uint32_t)dma_stream & 0xFFU) - 16U) / 24U;

    /* lookup table for necessary bitshift of flags within status registers */
    static const uint8_t flagBitshiftOffset[8U] = {0U, 6U, 16U, 22U, 0U, 6U, 16U, 22U};
    uint8_t stream_index = flagBitshiftOffset[stream_number];
    uint32_t stream_base;
    if(stream_number > 3U)
    {
    /* return pointer to HISR and HIFCR */
        stream_base = (((uint32_t)dma_stream & (uint32_t)(~0x3FFU)) + 4U);
    }else{
    /* return pointer to LISR and LIFCR */
        stream_base = ((uint32_t)dma_stream & (uint32_t)(~0x3FFU));
    }

    
    DMA_Base_Registers *regs = (DMA_Base_Registers *)(stream_base & (uint32_t)(~0x3FFU));
    /* Configure the source, destination address and the data length */
    
    /* Clear DBM bit */
    dma_stream->CR &=  ~DMA_SxCR_EN;
    dma_stream->CR |= DMA_SxCR_CHSEL_2 | // ch4
                        DMA_SxCR_MINC |     // DMA_MINC_ENABLE
                        DMA_SxCR_PL_1 |     // Priority level: High
                        DMA_SxCR_DIR_0;     // MEM TO PERIPH
                        // DMA_SxCR_PSIZE |  // DMA_PDATAALIGN_BYTE
                        // DMA_SxCR_MSIZE_|  // DMA_MDATAALIGN_BYTE
                        // DMA_SxCR_CIRC |     // Circular mode 
                        // DMA_FIFOMODE_DISABLE
                        // DMA_PINC_DISABLE;

    /* Configure DMA Stream data length */
    dma_stream->NDTR = DataLength;

    /* Peripheral to Memory */
    /* Configure DMA Stream destination address */
    dma_stream->PAR = periph_address;

    /* Configure DMA Stream source address */
    dma_stream->M0AR = mem_address;

    /* Clear all interrupt flags at correct offset within the register */
    regs->IFCR = 0x3FU << stream_index;
    
    /* Enable Common interrupts*/
    dma_stream->CR  |= DMA_SxCR_DMEIE;/*DMA_SxCR_TCIE | DMA_SxCR_TEIE |*/

    /* Enable the Peripheral */
    dma_stream->CR |= DMA_SxCR_EN;
}

void usart_on_dma_interrupt(DMA_Stream_TypeDef* dma_stream)
{
    // RX
    if(DMA1->LISR & DMA_LISR_TEIF1)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTEIF1;
        errors_on_error(DMA_TRANSFER_ERROR);
    }
    if(DMA1->LISR & DMA_LISR_FEIF1)
    {
        DMA1->LIFCR |= DMA_LIFCR_CFEIF1; // no fifo
        errors_on_error(DMA_TRANSFER_ERROR);
    }

    // no half transfer
    if(DMA1->LISR & DMA_LISR_TCIF1)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTCIF1;
    }

    // TX
    if(DMA1->LISR & DMA_LISR_TEIF3)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTEIF3;
    }
    if(DMA1->LISR & DMA_LISR_FEIF3)
    {
        DMA1->LIFCR |= DMA_LIFCR_CFEIF3;
    }
    // no half transfer
    // transfer complete
    if(DMA1->LISR & DMA_LISR_TCIF3)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTCIF3;
    }
}

void usart_transmit_dma(USART_TypeDef *USART, uint8_t* msg, uint16_t size)
{
    if(DMA1_Stream3->NDTR || !msg)
        return;

    dma_transmit(DMA1_Stream3, (uint32_t)msg, (uint32_t)&USART->TDR, size);

    SET_BIT(USART->ICR, USART_ICR_TCCF);

    SET_BIT(USART->CR3, USART_CR3_DMAT);
}

void mbport_init(   modbus_port_t* mbport,
                    USART_TypeDef *USART,
                    DMA_Stream_TypeDef *dma_input_stream,
                    uint32_t dma_input_stream_channel,
                    DMA_Stream_TypeDef *dma_output_stream,
                    uint32_t dma_output_stream_channel,
                    uint32_t baudrate,
                    circular_buffer_t* cbuffer,
                    modbus_device_t* mb_device,
                    GPIO_TypeDef* de_port,
                    uint16_t de_pin
                    )
{
	mbport->usart = USART;
    mbport->dma_input_stream = dma_input_stream;
    mbport->dma_input_stream_channel = dma_input_stream_channel;
    mbport->dma_output_stream = dma_output_stream;
    mbport->dma_output_stream_channel = dma_output_stream_channel;
    mbport->baudrate = baudrate;
    mbport->is_sending = false;
    mbport->cbuffer = cbuffer;
	mbport->device = mb_device;
	if(de_port && de_pin)
	{	
		mbport->de_port = de_port;
		mbport->de_pin = de_pin;
	}
    usart_init(mbport);
}
#endif

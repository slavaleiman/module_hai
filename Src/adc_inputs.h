#ifndef __ADC_INPUTS__
#define __ADC_INPUTS__ 1

#define U1_PORT		GPIOC		// ADC IN10
#define U1_PIN		GPIO_PIN_0
#define U2_PORT		GPIOC		// ADC IN11
#define U2_PIN		GPIO_PIN_1
#define U3_PORT		GPIOC		// ADC IN12
#define U3_PIN		GPIO_PIN_2
#define U4_PORT		GPIOC		// ADC IN13
#define U4_PIN		GPIO_PIN_3
#define U5_PORT		GPIOA		// ADC IN1
#define U5_PIN		GPIO_PIN_1
#define U6_PORT		GPIOA		// ADC IN3
#define U6_PIN		GPIO_PIN_3
#define U7_PORT		GPIOA		// ADC IN4
#define U7_PIN		GPIO_PIN_4

#define I1_PORT		GPIOA		// ADC IN5
#define I1_PIN		GPIO_PIN_5
#define I2_PORT		GPIOA		// ADC IN6
#define I2_PIN		GPIO_PIN_6
#define I3_PORT		GPIOA		// ADC IN7
#define I3_PIN		GPIO_PIN_7
#define I4_PORT		GPIOC		// ADC IN14
#define I4_PIN		GPIO_PIN_4
#define I5_PORT		GPIOC		// ADC IN15
#define I5_PIN		GPIO_PIN_5
#define I6_PORT		GPIOB		// ADC IN8
#define I6_PIN		GPIO_PIN_0
#define I7_PORT		GPIOB		// ADC IN9
#define I7_PIN		GPIO_PIN_1

#endif // __ADC_INPUTS__

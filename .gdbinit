
tar ext :4242
set print pretty on
set pagination off
set max-value-size 80000

define ktim
	set TIM12->CR1 &= ~1
end
define pu3
	p/t *USART3
end

define pt
	p/x *TIM5
end

define m
	p multimeter
end

define us
	p/x * USART3
end

define adc
	p/x *ADC
	p/x *ADC1
	p/x *ADC2
end

define dma
	p/x *DMA2
	p/x *DMA2_Stream0
	p/x *DMA1
	p/x *DMA1_Stream3
end

# print all data buffer

define pbuff 
	set $chan = 0
	set $count = 0
	printf "channel    : %d\n", $chan
	printf "data len   : %d\n", multimeter.data_len
	printf "seq len    :  %d\n", multimeter.seq_len
	printf "buffer     : %x\n", &multimeter.dma_buffer
	printf "buffer2    : %x\n", &multimeter.dma_buffer[multimeter.data_len]
	printf "actual data: %x\n", &multimeter.actual_data[$chan]

	#set $len = multimeter.data_len / multimeter.seq_len / 2
	set $len = multimeter.data_len / multimeter.seq_len
	printf "num samples: %d\n", $len
	while $count < $len
	  	printf "%x, ",  multimeter.actual_data[$count * multimeter.seq_len * 2 + $chan]
	  	set $count = $count + 1
	end
	printf "\n"
	printf "last addr : %x\n", &multimeter.actual_data[$count * multimeter.seq_len * 2 + $chan]
	
	set $count = 0
	set $len = multimeter.data_len / multimeter.seq_len / 2
	while $count < $len
	  	printf "%x, ", multimeter.actual_data[multimeter.data_len + $count * multimeter.seq_len * 2 + $chan]
	  	set $count = $count + 1
	end
	printf "last addr : %x\n", &multimeter.actual_data[multimeter.data_len + $count * multimeter.seq_len * 2 + $chan]
end

define pbuff2
	set $chan = 0
	set $count = 0
	printf "channel    :  %d\n", $chan
	printf "data len   : %d\n", multimeter.data_len
	printf "seq len    :  %d\n", multimeter.seq_len
	printf "buffer     : %x\n", &multimeter.dma_buffer
	printf "buffer2    : %x\n", &multimeter.dma_buffer[multimeter.data_len * 2]
	printf "actual data: %x\n", &multimeter.actual_data[$chan]
	
	printf "num samples: %d\n", multimeter.data_len / multimeter.seq_len / 2
	set $len = multimeter.data_len / multimeter.seq_len / 2
	while $count < $len
	  	printf "%x, ", multimeter.actual_data[multimeter.data_len + $count * multimeter.seq_len * 2 + $chan]
	  	set $count = $count + 1
	end
	printf "\n"
end

b HardFault_Handler

b multimeter.c:465
b multimeter.c:473
#b multimeter_on_adc_interrupt
display* DMA2_Stream0
r
pbuff

#include "unity.h"
#include <stdbool.h>
#include <stdint.h>
#include "sinus_table.h"
#include "filter_sma.h"
#include "filter_zerocross.h"
#include "filter_rms.h"
#include "multimeter.h"
#include <stdlib.h>

#define SINUS_TABLE_LEN     1024
#define PI 3.14159265358979323846

void fill_data(uint32_t phase_shift, uint8_t seq_len, uint8_t rank, uint16_t offset)
{
    uint16_t* buffer = multimeter_buffer();
    // buffer += 2331;
    // multimeter_set_actual_buffer(buffer);

    uint32_t count = 0;
    float u = 0.0;
    float step = (float)SINUS_TABLE_LEN / multimeter_phase_len();
    printf("sinval\n");
    for(uint32_t i = 0; i < MM_NUM_SAMPLES; ++i)
    {
        u += step;
        int16_t sinval = (sinus_table[(uint32_t)u % SINUS_TABLE_LEN] >> 4) + offset;
        buffer[seq_len * 2 * count + 2 * rank] = sinval;

        sinval = (sinus_table[((uint32_t)u + phase_shift) % SINUS_TABLE_LEN] >> 4) + offset;
        buffer[seq_len * 2 * count + 2 * rank + 1] = sinval;
        printf("%d  ", sinval);
        ++count;
    }
    printf("count=%d\n", count);
}

void print_buff(uint8_t seq_len, uint8_t rank)
{
    uint16_t *buff = multimeter_buffer();
    for(uint16_t u = 0; u < 2 * seq_len * MM_NUM_SAMPLES; u += 1)
    {
        printf("%u:%u ", u, buff[u + rank]);
    }
    printf("\n");
}

void test_average(void)
{
    uint16_t phase_shift = 0;
    multimeter_init(NULL);

    multimeter_set_rate(5000);

    multimeter_output_configure(0, 0, 7);
    multimeter_output_configure(1, 1, 8);
    multimeter_output_configure(2, 2, 3);
    multimeter_output_configure(3, 2, 3);
    multimeter_output_configure(4, 2, 3);
    multimeter_output_configure(5, 2, 3);
    multimeter_update_configure();

    // multimeter_set_pair_channel(5, 0, 6);
    // multimeter_update_configure();

    uint16_t seq_len = multimeter_seq_length();
    printf("seq_len:%d\n", seq_len);

    float phase_divider = phase_shift ? (float)SINUS_TABLE_LEN / phase_shift : 1;
    float expected_cosFi = phase_divider ? cosf(2 * PI / phase_divider) : 1;

    uint16_t offset = 100;
    multimeter_set_adc_zero_level(2048 + offset); 
    // fill channel 0 and 1 
    fill_data(100, seq_len, 0, offset);
    fill_data(100, seq_len, 1, offset);
    fill_data(300, seq_len, 2, offset);
    fill_data(300, seq_len, 3, offset);
    fill_data(300, seq_len, 4, offset);
    fill_data(300, seq_len, 5, offset);
    fill_data(1024, seq_len, 6, offset); // vbat

    multimeter_update();

    print_buff(seq_len, 0);

    float uavg0 = multimeter_uavg(0);
    printf("avg0 = %f\n", uavg0);
    TEST_ASSERT_EQUAL(0, uavg0);

    float uavg1 = multimeter_uavg(1);
    printf("avg1 = %f\n", uavg1);
    TEST_ASSERT_EQUAL(0, uavg1);

    float urms = multimeter_urms(0);
    printf("rms = %f <> %f\n", urms, 2047 / sqrt(2));
    TEST_ASSERT_EQUAL((float)2047 / sqrt(2), urms);
}

void test_cosFi(float phase_shift, uint16_t voltage_offset)
{
    multimeter_init(NULL);
    multimeter_set_phase_len(MM_NUM_SAMPLES);

    multimeter_output_configure(0, 0, 7);
    multimeter_update_configure();

    uint8_t seq_len = multimeter_seq_length();
    printf("seq_len = %d\n", seq_len);

    float phase_divider = (phase_shift) ? (float)SINUS_TABLE_LEN / phase_shift : 1;
    float expected_cosFi = (phase_divider) ? cosf(2 * PI / phase_divider) : 1;
 
    fill_data(phase_shift, seq_len, 0, 100);
    // fill_data(phase_shift, seq_len, 1); // vbat

    uint16_t offset = 100;
    multimeter_set_adc_zero_level(2048 + offset);  

    multimeter_update();  

    float S = multimeter_S(0); // Urms * Irms; // полная мощность
    int32_t P = multimeter_P(0); // мгновенная мощность

    printf("phase shift = %f\n", phase_shift);
    printf("phase divider = %f\n", phase_divider);
    printf("S = %f\n", S);
    printf("P = %d\n", P);
    printf("avg = %f\n", multimeter_uavg(0));
    printf("rms = %f\n", multimeter_urms(0));

    float calc_cos = multimeter_cosFI(0);
    // float calc_cos;
    // if(P && S)
        // calc_cos = P / S;
    printf("cosf = %f\n", calc_cos);
    printf("expcos = %f\n", expected_cosFi);
    printf(">>>>>>>>>  >>>>>>>>>>>  delta %d\n\n", abs((int)(expected_cosFi * 1000.0 - calc_cos * 1000.0)));

    TEST_ASSERT_LESS_OR_EQUAL(1, (uint32_t)abs((int)(expected_cosFi * 1000.0 - calc_cos * 1000.0)));
}

void test_cos_phases(void)
{
    // test_cosFi(0); 
    // test_cosFi(1); 
    // test_cosFi(2); 
    // test_cosFi(3); 
    // test_cosFi(4); 
    // test_cosFi(5); 
    // test_cosFi(6); 
    // test_cosFi(7); 
    // test_cosFi(8); 
    // test_cosFi(9); 
    // test_cosFi(10);
    // test_cosFi(11);

    uint16_t voltage_offset = 10;

    test_cosFi(20, voltage_offset);
    test_cosFi(50, voltage_offset);
    test_cosFi(90, voltage_offset);
    test_cosFi(100, voltage_offset);
    test_cosFi(210, voltage_offset);
    test_cosFi(1024.0 / 3, voltage_offset);

    for(uint8_t i = 0; i < 120; ++i)
    {
        test_cosFi(256 / 3, voltage_offset);
    }
}

void setUp(void)
{
}

void tearDown(void)
{
}

int main(void)
{
    UNITY_BEGIN();
    // RUN_TEST(test_average);
    // RUN_TEST(test_cos_phases);

    uint16_t buff[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printf("buf   :   %x\n", (uint32_t)buff);
    printf("buf[0]:   %x\n", (uint32_t)&buff[0]);
    printf("buf[1]:   %x\n", (uint32_t)&buff[1]);
    printf("buf[2]:   %x\n", (uint32_t)&buff[2]);
    printf("buf[3]:   %x\n", (uint32_t)&buff[3]);
    printf("buf[4]:   %x\n", (uint32_t)&buff[4]);

    return UNITY_END();
}

#include "unity.h"
#include "circular_buffer.h"
#include "string.h"

typedef struct{
	uint32_t ts; // timestamp millisec
	uint32_t id;
	uint8_t data[8];
	uint16_t len;
}__attribute__((packed)) j1939_message_t ;

void setUp()
{

}

void tearDown(void)
{
}

CIRCULAR_BUFFER_DEF(can1_cbuffer, 67 * sizeof(j1939_message_t))

void print_cbuffer(void)
{
	printf("head:\t%x\ntail:\t%x\nbuffer:\t%x\nend:\t%x\ncount:\t%d", 
					(uint32_t)can1_cbuffer.head,
					(uint32_t)can1_cbuffer.tail, 
					(uint32_t)can1_cbuffer.buffer, 
					(uint32_t)can1_cbuffer.buffer_end,
					(uint32_t)can1_cbuffer.count
					);	
} 

void test_can_circular_buffer(void)
{
	message_t mess;
	message_t in_mess;

	uint8_t input[16];
	uint8_t output[16];

	uint32_t pop_len = 0;
	uint32_t push_len = 0;

	uint8_t data_len = 1;

	for(uint8_t i = 0; i < 255; ++i)
	{
		printf(">>>>>\n");
		for(uint8_t y = 0; y < 15; ++y)
		{
			++data_len;
			data_len %= 8;
			mess.data = can1_cbuffer.head + sizeof(message_t);
			mess.len = 8 + data_len;

			if(!cbuffer_push(&can1_cbuffer, (uint8_t*)&mess, sizeof(message_t))) // TODO 
			{
				push_len += sizeof(message_t);
			}else{
				printf("PUSH ERROR\n");
				return;
			}
			uint32_t acab = 0xACAB; 
			memcpy(output, &acab, 4);
			memcpy(&output[4], &acab, 4);

			for(uint8_t d = 0; d < data_len; ++d)
				output[8 + d] = d + y;

			for(uint8_t j = 0; j < mess.len; ++j)
				printf("%x ", output[j]);
			printf("\n");

			if(!cbuffer_push(&can1_cbuffer, (uint8_t*)output, 8 + data_len))
			{
				push_len += 8 + data_len;
			}else{
				printf("PUSH2 ERROR\n");
				return;
			}
		}

		printf("buf count=%d \n", (int)can1_cbuffer.count);
		printf("<<<<<<<\n");

		for(uint8_t y = 0; y < 15; ++y)
		{
			if(!cbuffer_pop(&can1_cbuffer, (uint8_t*)&in_mess, sizeof(message_t)))
				pop_len += sizeof(message_t);
			else{
				printf("POP ERROR\n");
				return;
			}
			uint16_t len = in_mess.len;
			if(len > 16)
			{
				print_cbuffer();
				printf("ERROR LEN > 16\n");
				return;
			}
			// printf("pop len=%d \n", (int)len);
			if(!cbuffer_pop(&can1_cbuffer, (uint8_t*)&input, len))
				pop_len += len;
			else{
				printf("POP ERROR\n");
				return;
			}
			for(uint8_t j = 0; j < len; ++j)
				printf("%x ", input[j]);
			printf("\n");
		}

		printf("buf count=%d \n", (int)can1_cbuffer.count);
	}
	TEST_ASSERT_EQUAL(pop_len, push_len);
}

void test_cbuffer_inout(void)
{
	message_t mess;
	message_t in_mess;

	uint8_t input[16];
	uint8_t output[16];

	uint32_t pop_len = 0;
	uint32_t push_len = 0;

	uint8_t data_len = 1;

	printf(">>>>>\n");
	for(uint8_t y = 0; y < 3; ++y)
	{
		++data_len;
		data_len %= 8;
		mess.data = can1_cbuffer.head + sizeof(message_t);
		mess.len = 8 + data_len;

		if(!cbuffer_push(&can1_cbuffer, (uint8_t*)&mess, sizeof(message_t))) // TODO 
		{
			push_len += sizeof(message_t);
		}else{
			printf("PUSH ERROR\n");
			return;
		}
		uint32_t acab = 0xACAB; 
		memcpy(output, &acab, 4);
		memcpy(&output[4], &acab, 4);

		for(uint8_t d = 0; d < data_len; ++d)
			output[8 + d] = d + y;

		for(uint8_t j = 0; j < mess.len; ++j)
			printf("%x ", output[j]);
		printf("\n");

		if(!cbuffer_push(&can1_cbuffer, (uint8_t*)output, 8 + data_len)) // TODO 
		{
			push_len += 8 + data_len;
		}else{
			printf("PUSH2 ERROR\n");
			return;
		}
	}
	printf("buf count=%d \n", (int)can1_cbuffer.count);
	printf("<<<<<<<\n");
	for(uint8_t y = 0; y < 3; ++y)
	{
		if(!cbuffer_pop(&can1_cbuffer, (uint8_t*)&in_mess, sizeof(message_t)))
			pop_len += sizeof(message_t);
		else{
			printf("POP ERROR\n");
			return;
		}
		uint16_t len = in_mess.len;
		if(len > 16)
		{
			print_cbuffer();
			printf("ERROR LEN > 16\n");
			return;
		}
		// printf("pop len=%d \n", (int)len);
		if(!cbuffer_pop(&can1_cbuffer, (uint8_t*)&input, len))
			pop_len += len;
		else{
			printf("POP ERROR\n");
			return;
		}
		for(uint8_t j = 0; j < len; ++j)
			printf("%x ", input[j]);
		printf("\n");
	}
	printf("buf count=%d \n", (int)can1_cbuffer.count);

	TEST_ASSERT_EQUAL(pop_len, push_len);
}


void test_cbuffer_message(void)
{
	CIRCULAR_BUFFER_DEF(cbuff, 136 * 5);
	uint8_t input[128];
	uint8_t output[128];

	uint32_t pop_len = 0;
	uint32_t push_len = 0;
	uint32_t to_push_len = 128;
	for(uint16_t d = 0; d < to_push_len; ++d)
	{
		output[d] = (d * 213) % 255;
	}

	for(uint8_t u = 0; u < 5; ++u)
	{
		if(cbuffer_push_message(&cbuff, output, to_push_len))
		{
			printf(">>>>>>>>>>      push error     <<<<<<<<<\n");
			break;
		}
		push_len += to_push_len;
	}

	while(1)
	{
		int16_t len = cbuffer_pop_message(&cbuff, input);
		if(len > 0)
		{
			pop_len += len;
		}else if(len < 0){
			printf("<<<<<<<<<<<<     pop error    >>>>>>>>>>>>\n");
			break;
		}else{
			break;
		}
	}
	printf("poplen %u\n", pop_len);
	TEST_ASSERT_EQUAL(pop_len, push_len);
}

void test_queue(void)
{

// TEST
// при приеме сравнивать адрес пришедшего элемента и сранивать с head
//  если они не совпадут - всё 3,14здец
	// вытаскивай следующий, пока не совпадут.

// для буфера переменной длины эта зависимость тоже работает
}

int main(void)
{
    UNITY_BEGIN();
    // RUN_TEST(test_cbuffer_inout);
    // RUN_TEST(test_can_circular_buffer);
    RUN_TEST(test_cbuffer_message);
    return UNITY_END();
}
